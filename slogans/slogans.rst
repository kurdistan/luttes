.. index::
   ! Slogans
   ! Femme, Vie, Liberté
   ! Jin Jîyan, Azadî (kurde)
   ! Woman, Life, Freedom (anglais)
   ! Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan)
   ! FemmeVieLiberte
   ! ZanZendegiÂzâdi
   ! JinJiyanAzadî
   ! WomanLifeFreedom

.. _slogans:

=====================
Slogans
=====================


Hastags
=========

Pour notre liberté et pour la votre" disaient les révoltés du ghetto de Varsovie,
pour votre liberté et pour la notre" répétaient les dissidents russes de la Place Rouge en 68

**Ce qui se passe au Kurdistan nous concerne aussi.**

- #NousSommesLeursVoix #zhinaAmini #JinaMahsaAmini #MasahAmini #MasahAmini #Masah_Amini #Jîna_Emînî #Jina_emini #OpIran #مهساامینی
- #EndIranRegime #IranProtests #LetUsTalk #Jîna_Emînî #Jina_emini #WhiteWednesdays #TwitterKurds #مهسا_امینی #Rojhilat #JinaMahsaAmini #opiran
- #EndIranRegime #IranProtests #LetUsTalk #Jîna_Emînî #Jina_emini #WhiteWednesdays #TwitterKurds #مهسا_امینی #Rojhilat #MahsaAmini #OpIran
- #IranRevolution #IranProtests2022 #ZanZendegiÂzâdi #JinJiyanAzadî #WomenLifeFreedom #FemmesVieLiberte

::

    #Grenoble #IranRevolution #IranProtests2022 #NousSommesLeursVoix #EndIranRegime #IranProtests
    #zhinaAmini #JinaMahsaAmini #MasahAmini #MasahAmini #Masah_Amini #OpIran
    #FemmeVieLiberte #ZanZendegiÂzâdi #JinJiyanAzadî #WomanLifeFreedom


.. _femmes_vie_liberte:

Femmes, Vie, Liberté (Français) #FemmesVieLiberte
===================================================

**Femmes, Vie, Liberté (Français)**


Woman, Life, Freedom (Anglais) #WomanLifeFreedom
-------------------------------------------------------

**Woman, Life, Freedom (Anglais)**

Jin Jîyan, Azadî (kurde) #JinJiyanAzadî
---------------------------------------------

**Jin Jîyan, Azadî (Kurde)**


.. _zan_zendegi_azadi:

Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi
--------------------------------------------------------------------

**Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan)**


- :ref:`iran_luttes:femme_vie_liberte_2022_09_24`


.. _mort_au_dictateur:

Mort au dictateur (français)
==============================

Mort au dictateur (français)

Bimre dîktator (kurde)
--------------------------

Bimre dîktator (kurde)
