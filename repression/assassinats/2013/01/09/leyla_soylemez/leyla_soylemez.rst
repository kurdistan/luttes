.. index::
   ! Leyla Söylemez

.. _leyla_soylemez:

====================================================================================
**Leyla Söylemez** encadrait le mouvement de jeunesse du parti |LeylaSoylemez|
====================================================================================

- https://fr.wikipedia.org/wiki/Leyla_S%C3%B6ylemez


.. figure:: images/leyla_soylemez.jpg
   :align: center

Introduction
=============

Leyla Söylemez, née le 1er janvier 1988 à Mersin (Turquie) est une
activiste kurde, assassinée le 9 janvier 2013 dans le 10e arrondissement
de Paris.

Leyla Söylemez est née en 1989 dans le district de Lice, dans la province
de Diyarbakır, au Kurdistan de Turquie.
Elle passe son enfance à Mersin. Sa famille s'installe en Allemagne au
cours des années 1990 et se fixe à Halle. Alors qu'elle entreprend des
études d'architecture, Leyla Söylemez commence à s'engager pour la cause
kurde.

À partir de 2006, elle milite dans les rangs des organisations de la
jeunesse kurde dans différentes villes d'Europe, notamment à Berlin,
Cologne, Hanovre, Francfort et à Bâle.

En 2012, elle est envoyée à Paris, pour contribuer aux activités du
Centre d'information du Kurdistan.


