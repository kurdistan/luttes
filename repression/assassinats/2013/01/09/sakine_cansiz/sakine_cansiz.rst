.. index::
   ! Sakine Cansız

.. _sakine_cansiz:

========================================================================
**Sakine Cansız** dirigeante et cofondatrice du PKK |SakineCansiz|
========================================================================

- https://fr.wikipedia.org/wiki/Sakine_Cansiz


.. figure:: images/sakine_cansiz.png
   :align: center

Introduction
==============

Sakine Cansiz (orthographié en kurde Sakîne Cansiz, en turc Sakine Cansız,
[saˈkine ˈdʒɑnsɯz]), née le 12 février 1958 dans l'actuelle province de
Tuncelinote au Kurdistan de Turquie 3 et assassinée à Paris 10e le 10 janvier 2013
est une militante kurde de nationalité turque, et l'une des fondatrices
du Parti des travailleurs du Kurdistan (PKK).

