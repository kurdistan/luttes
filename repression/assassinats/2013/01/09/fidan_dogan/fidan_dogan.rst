.. index::
   ! Fidan Doğan

.. _fidan_dogan:

========================================================================
**Fidan Doğan** surnommée "la Diplomate" |FidanDogan|
========================================================================

- https://fr.wikipedia.org/wiki/Fidan_Do%C4%9Fan
- https://fr.wikipedia.org/wiki/Triple_assassinat_de_militantes_kurdes_%C3%A0_Paris

.. figure:: images/fidan_dogan.png
   :align: center


Introduction
===============

- https://fr.wikipedia.org/wiki/Fidan_Do%C4%9Fan

Fidan Doğan, née le 17 janvier 1984 à Elbistan en Turquie et assassinée
à Paris 10e, en France, le 10 janvier 2013, était une militante kurde,
membre du Parti des travailleurs du Kurdistan.


Biographie
===============

Fidan Doğan est née le 17 janvier 1984 dans le district d'Elbistan, dans
la province de Marash, au Kurdistan de Turquie.

Alors qu'elle est très jeune, sa famille s'installe en France, d'abord
à Lyon, puis à Strasbourg. À partir de 1999, elle commence à s'engager
activement pour la cause kurde. Elle prend part aux activités des organisations
de jeunesse et des associations féminines kurdes, où elle est connue
sous le nom de code de « Rojbîn ».

En 2002, elle est affectée aux activités diplomatiques du mouvement.
C'est ainsi qu'elle devient représentante du Congrès national du Kurdistan
(KNK), une structure basée à Bruxelles, ainsi que responsable du Centre
d'information du Kurdistan à Paris, un bureau considéré comme étant
« une antenne officieuse » du Parti des travailleurs du Kurdistan (PKK).

Assassinat
===========

Article détaillé : `Triple assassinat de militantes kurdes à Paris <https://fr.wikipedia.org/wiki/Triple_assassinat_de_militantes_kurdes_%C3%A0_Paris>`_

Fidan Doğan est assassinée à Paris au centre d'information du Kurdistan,
147 rue la Fayette, dans le Xe arrondissement, dans la nuit du 9 au 10
janvier 20135, en même temps que deux autres militantes kurdes,
Sakine Cansiz et Leyla Söylemez.

Selon la justice française, les responsables du triple assassinat pourraient
être les services secrets turcs, le MİT6. L'assassin présumé, Omer Güney
est un Turc de 34 ans, qui avait travaillé en tant qu'agent d'entretien
à l'aéroport de Paris-Charles-de-Gaulle. Surtout, il est le chauffeur
et l’homme à tout faire des trois victimes. Lors de l'enquête, les experts
le désignent comme un « familier » et un « professionnel ».

Un reportage de TFI montre qu’après l'assassinat il est l'une des
premières personnes accourues au pied de l’immeuble.

Quelques mois après ces assassinats, un enregistrement audio d’une
conversation entre Ömer Güney et des agents des services secrets turcs (MIT),
ainsi que des notes, sont mis en ligne de manière anonyme.
Il s'est avéré que le téléphone portable d'Omer Güney recelait des
photos de centaines de militants kurdes.

La juge d'instruction antiterroriste Jeanne Duyé est chargée de l’enquête.
En septembre 2013, l'ordinateur de la magistrate est volé à son domicile
lors d’un cambriolage mystérieux. De même, un projet d'évasion d'Omer Güney
est déjoué. Celui-ci, incarcéré depuis le 21 janvier 2013 près de Paris,
aurait compté s'évader « avec l'aide d'un membre du MIT ».

La magistrate, au-delà de la possible implication des services secrets
turcs, n'a pas réussi à prouver qui étaient les commanditaires de
l'assassinat, ni s'ils ont agi « avec l'aval de leur hiérarchie »,
ni s'ils l'ont fait « à l'insu de leur service afin de le discréditer
ou de nuire au processus de paix » entamé à l'époque entre Ankara et le PKK.

En outre, les autorités turques refusent de répondre aux demandes de la
justice française.

Le 13 décembre 2016, Ömer Güney est transporté d’urgence du centre
pénitentiaire de Fresnes à l'hôpital de la Salpêtrière. Souffrant depuis
longtemps d’un cancer du cerveau, il est contaminé par la légionellose.
Il meurt d’une pneumonie le 17 décembre, cinq semaines avant le début
de son procès.

Eyyup Doru, représentant en Europe du parti pro-kurde Parti démocratique
des peuples (HDP), déclare que la cible principale de l'assassinat était
Fidan Doğan, et que c'est aussi la France qui était visée à travers elle.

Selon lui, c'est tout d'abord la bonne entente entre cette militante et
les autorités françaises qu'on aurait voulu frapper. La jeune femme,
« très intelligente, selon l’avocate Sylvie Boitel, et qui possédait
fait rarissime, les codes des trois cultures : turque, kurde et française»,
aurait, selon l’une de ses amies, « rencontré le Président François Hollande
au moins trois fois alors qu'il était secrétaire du Parti socialiste »
et entretenait de nombreux liens avec les autorités politiques françaises.

De plus, l’assassin a achevé son crime en tirant une balle dans la bouche
de Fidan Dogan.


