.. index::
   ! Mîr Perwer
   ! Şirin Aydın

.. _miran_perwer:
.. _mir_perwer:

=============================================================================================
**Mîr Perwer (Şirin Aydın  son vrai nom), membre du Mouvement Culturel Kurde** |MirPerwer|
=============================================================================================

- https://kurdistan-au-feminin.fr/2022/12/24/massacre-paris-soutenir-les-artistes-kurdes/
- https://inv.riverside.rocks/watch?v=fa5H-P6iV88

.. figure:: images/mir_perwer.jpg
   :align: center

.. figure:: images/mir_perwer_banderolle.jpg
   :align: center


La deuxième victime est **Mîran Perwer**, un chanteur bien connu dans la
communauté, originaire de Muş-Varto au Bakur (Kurdistan du Nord).

Il avait du fuir la Turquie où il était poursuivi pour ses activités
politiques et notamment son soutien au HDP.


Article de serhildan
========================

- https://serhildan.org/evin-goyi-mir-perwer-abdurrahman-kizil-trois-portraits/

Son art en défense d’un avenir libre
-----------------------------------------

Mîr Perwer était son nom de scène, Şirin Aydın son nom de naissance.

Cet homme, tombé martyr lors de l’attaque armée à Paris, avait grandi
avec les mélodies des dengbêj (conteurs chantants) kurdes dès son enfance.

Il était un artiste kurde fermement engagé dans les valeurs artistiques
de libération créées par le PKK.

Mîr Perwer est né en 1993 dans le quartier de Gimgim à Mûş [tout au nord du Kurdistan].
Dans les années 90, sa famille a été contrainte de migrer vers le centre
de Mûş en raison de l’oppression de l’État turc.

Dès son enfance, il s’est consacré à son pays, le Kurdistan, et à la
musique kurde. Il s’est opposé au système scolaire en Turquie et a
quitté l’école, ne voulant pas étudier dans celle du colonialisme.

Il a néanmoins réussi à s’instruire par ses propres moyens.

En apprenant à connaître la lutte pour la libération du Kurdistan, il a
appris à mieux se connaître lui-même.
Il a pris le martyr Hozan Serhed [1] comme exemple dans son travail pour
la culture et l’art kurde contre les politiques d’assimilation.

« Mîr Perwer faisait partie de ces artistes kurdes qui ont du prendre la
route de l’exil à cause de leur attachement à leur langue et à leur culture. »

Mîr Perwer est devenu l’une des voix de la musique kurde. Il a été arrêté
par l’État génocidaire turc pour son travail.

Il a passé plus de 4 ans en prison. Pendant son emprisonnement, sa conscience
de la réalité des gens et de son identité propre s’est approfondie.
Il a transformé la prison en un lieu de production et y a écrit nombre
de ses chansons.

Fin 2018, à sa sortie de prison, il s’est davantage concentré sur ses œuvres
artistiques. Sa première œuvre s’appelle « Bajar ».

Cette chanson a été très appréciée par le public. Il a aussi participé
activement à soutenir la campagne électorale du HDP de 2018 à Mûş.

En 2021, lorsque l’État turc occupant a condamné Mîr Perwer à 18 ans de
prison, il a dû partir pour l’Europe. Il a continué son combat là où il
l’avait laissé et a continué avec la même attitude politique et position
artistique à Paris.

Quelques jours avant de tomber martyr à Paris, il chantait à l’anniversaire
du PKK à Rennes.

Jusqu’au moment où il a perdu la vie, il a été un combattant de la liberté
de son peuple et de la musique kurde.



Article de Libération du 26 décembre 2022
===========================================

- https://www.liberation.fr/societe/police-justice/attaque-raciste-contre-les-kurdes-emine-kara-a-protege-la-france-la-france-ne-la-pas-protegee-20221226_ZGQTQOHM35DBRDNMONOADKY3QA/


**Mir Perwer** avait choisi la France «pour échapper à la censure»
Une vidéo postée sur Instagram montre la deuxième victime devant la tombe
d’Ahmet Kaya au Père-Lachaise, entonnant un chant poignant en hommage au
célèbre chanteur kurde, mort le 16 novembre 2000 à Paris où il s’était
lui aussi exilé : Mir Perwer pouvait-il imaginer qu’il serait tué un mois
plus tard à quelques mètres du centre culturel qui porte le nom de la
gloire kurde ?

Né en 1993, Sirin Aydin, de son vrai nom, était originaire de la ville
kurde de Mus, en Anatolie orientale. En France depuis moins de trois ans,
il avait obtenu récemment le statut de réfugié après le « procès qui a
été ouvert en Turquie contre lui », précise-t-on du côté du CDKF.

Il avait choisi « l’Europe, et en particulier la France, pour échapper
à la censure, l’autocensure et la difficulté de pratiquer un art kurde
en Turquie ».

Unique garçon parmi six sœurs, il était marié et père d’un enfant de 6 ans,
qu’il essayait de faire venir en France


Ignoble de la part de l'Etat voyou et inhumain turque
=========================================================

- http://bird.trom.tf/Berivanfirat75/status/1611643246769500161#m

Le corps de l'artiste #kurde #MirPerwer tué à #Paris a été enlevé par les
forces #turques et voilà comment il est transporté au regard de sa famille,
de ceux qu'ils l'aiment.

Et, il ya encore des personnes indécentes qui ne demandent, mais que
vous a fait  la Turquie⁉️


.. figure:: images/dans_une_benne.png
   :align: center


Nous venons d'apprendre le décès de Filiz Aydın, la mère de Mîr Perwer.
============================================================================

- http://bird.trom.tf/femmeskurdesTJK/status/1615390633749012482#m

Nous venons d'apprendre le décès de Filiz Aydın, la mère de Mîr Perwer.

Elle avait été hospitalisée après les funérailles de son fils, fortement réprimées par les autorités turques.

Paix à son âme,
Notre vengeance sera la révolution des femmes!


