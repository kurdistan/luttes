.. index::
   ! Emine Kara
   ! Evîn Goyî

.. _emine_kara:

=========================================================================
**Emine Kara** (Evîn Goyî et Heval Evîn ses noms de guerre) |EmineKara|
=========================================================================


#JesuisKurde #Paris10 #ParisAttacks #TwitterKurds #AttentatTerroriste #Paris #ParisMassacreOfKurds

UNE MILITANTE KURDE TUÉE A PARIS

.. figure:: images/emine_kara.jpg
   :align: center


.. figure:: images/emine_kara_2.jpg
   :align: center


.. figure:: images/emine_kara_3.jpg
   :align: center


.. figure:: images/emine_kara_4.jpg
   :align: center

.. figure:: images/je_suis_evin.jpg
   :align: center


Emine Kara était une combattante dans la guerre contre DAECH / ISIS au Rojava.

Elle était venue à Paris pour des soins et elle a été tuée lors de
l'attentat terroriste d'aujourd'hui.

Emine Kara était la responsable du Mouvement des femmes kurdes en France.

Elle était celle dont la France avait refusé la demande d’asile.

Elle se démenait depuis plusieurs mois pour organiser la solidarité avec la
révolution des femmes en Iran ou encore les mobilisations du 25 novembre.

Source: https://twitter.com/KurdistanAu/


One of the murdered Kurds in #Paris is Emine Kara, nom de guerre Evin Goyi,
one leading figure of the Kurdish women's liberation movement.

One of those who created the notion of #JinJiyanAzadi with their struggle
for freedom under the most difficult circumstances. #JeSuisKurde


Article de Kurdistan au féminin
==================================

- https://kurdistan-au-feminin.fr/2023/01/01/evin-goyi/


La militante kurde tuée lors de l’attaque terroriste du 23 décembre à Paris,
Evîn Goyî a joué un rôle central dans la participation des femmes à la
révolution du Rojava. Elle a consacré sa vie à la lutte. Elle a également
insisté sur la place de la femme libre contre la brutalité de l’EI.

Evîn Goyî (Emine Kara) est née dans le village de Hilal, au Kurdistan
du Nord. Elle a rejoint les rangs du PKK en 1988.

Elle s’est battue contre le groupe terroriste misogyne DAECH au Rojava et
a été blessée dans cette guerre. (Le chanteur Mir Perwer et l’activiste
Abdurrahman Kızıl ont été tués avec Evîn Goyi, lors du massacre qui a eu
lieu à Paris le 23 décembre 2022.)

A la tête des communes et des assemblées
---------------------------------------------------

Cahid Hesen, qui a rencontré Evîn Goyî en 2011 au Kurdistan du Sud, a
déclaré : « En 2011, j’ai eu la chance de rencontrer Heval (camarade)
Evîn, bien que brièvement.
Dans les premiers mois de 2016, nous étions dans la même zone au Rojava
et participions à la même administration. Sans aucun doute, Heval Evîn
a joué un grand rôle de premier plan dans la révolution du Rojava et
de la Syrie du Nord et de l’Est.
Elle a mené des activités pour les communes et les assemblées au sein
de TEV-DEM [Mouvement de la société démocratique]. »

Elle a une place dans le cœur des Arabes
----------------------------------------------

Elle a gagné une place dans le cœur des habitants du Rojava en peu de
temps car elle était une camarade très sociable.
Elle a gagné une place non seulement parmi le peuple kurde, mais aussi
parmi les Arabes et d’autres composantes. Elle a assumé le rôle principal
des villages aux villes du Kurdistan du Rojava et a construit des communes
et des assemblées de ses propres mains.
Elle a également assumé le rôle de premier plan au sein des Forces de
la défense sociale.

Affirmant qu’Evîn Goyî était connue pour sa modestie, Cahid évoque ainsi
son rôle dans la révolution du Rojava : « Heval Evîn était une pionnière.
Sa conversation était profonde et elle était une amie compréhensive.
Elle n’avait de problèmes avec personne et essayait toujours d’écouter
et de comprendre l’autre personne. »

Elle a lutté pour une Syrie démocratique
----------------------------------------------

Elle a beaucoup réfléchi à la poursuite du développement de la révolution
du Rojava, à son statut exemplaire au Kurdistan et à la manière de contribuer
à la création d’une Syrie démocratique.

A la tête de la lutte des femmes pendant 34 ans
---------------------------------------------------

Cahid fait référence aux 34 années de lutte d’Evîn Goyî, affirmant
qu’elle « a consacré sa vie à la lutte pour la liberté. Sans aucun doute,
elle a également dirigé la lutte pour la liberté des femmes dans la
révolution du Rojava.
Bien que sa santé se soit détériorée vers la fin de 2018, elle a continué
à travailler. Heval Evîn a combattu dans 4 parties du Kurdistan.

Elle a donné l’exemple aux femmes du Moyen-Orient et est devenu un
symbole de résistance aux peuples opprimés, aux peuples qui luttent
pour la démocratie et la liberté (…). »

« La France devrait clarifier le massacre ou elle deviendra complice de ce crime »
-------------------------------------------------------------------------------------

Attirant l’attention sur le 2e massacre de Paris, qui a coïncidé avec
l’anniversaire de l’assassinat de Sakine Cansiz, Fîdan Dogan et Leyla Saylemez,
également assassinées à Paris le 9 janvier 2013, Cahid Hesen a déclaré :
« La France ne doit pas couvrir le massacre, elle doit vérité. Si l’Etat
turc et le MIT sont à l’origine de ce massacre, la France sera également
complice si le massacre du 23 décembre n’est pas élucidé.
Nous continuerons à nous battre pour que le massacre ne soit pas couvert. »

« Une amie précieuse »
------------------------------

Kelsuma Eyup a déclaré avoir rencontré Evîn Goyî à Dirbesiyê en 2018 et
a décrit ces jours comme suit : « Elle tenait des réunions tout le temps,
les gens du quartier étaient fidèles à Heval Evîn. Elle était une amie
précieuse. (…) »

Kelsuma a ajouté: «Elle était aussi à l’aise dans notre maison que si
c’était la sienne. Nous avons ouvert notre maison et nos cœurs à Heval Evîn. (…)
Mes 5 enfants aimaient beaucoup Heval Evîn. Ils demandaient toujours
quand elle reviendrait. »


« Nous protégerons son travail »
--------------------------------------

Kelsuma a déclaré : « Heval Evîn était avant-gardiste. Elle a gagné une
place dans le cœur des gens en peu de temps. Sa mort nous a attristés.

Ces jours ont passé devant mes yeux comme un film et tout le monde a été
choqué. Nous allons intensifier notre lutte pour protéger son travail. »

Elle a donné du courage aux autres
---------------------------------------

Evîn Seydo raconte avoir fait la connaissance d’Evîn Goyî lorsqu’elle
est venue chez elle en 2017. « Son discours était naturel, elle riait
toujours. Même lorsqu’elle ne venait pas chez nous, nous allions tout
de suite dans n’importe quelle maison où elle se rendait.

Parce que nous aimions écouter Heval Evîn, c’était stimulant.

Si les femmes sont fortes aujourd’hui, résistantes et dirigeantes,
c’est aussi grâce à Heval Evîn. Elle nous a donné du courage.
Elle influençait les gens et les mobilisait. Nous avons promis de
continuer le combat. Nous élèverons nos enfants dans la lutte. »

« Son silence était instructif »
--------------------------------------

Mihemed Xelef a dit qu’il est resté avec Evîn Goyî pendant 3 ans et a
ajouté : « Elle ne parlait pas beaucoup, elle nous a entraînés avec
son silence. Son silence était instructif. Elle a créé une loyauté
spirituelle parmi le peuple. (…) »

Soulignant qu’Evîn Goyî représente la ligne des femmes libres, Mihemed
s’est ensuite adressée aux segments qui luttent pour la démocratie et
la liberté et a déclaré : « Les femmes ont mené la révolution du Rojava.
L’une de ces femmes était Heval Evîn. Une fois de plus, une femme
a été visée.

Le silence des puissances internationales indique une complicité dans le
massacre. Notre appel ne s’adresse pas aux puissances internationales,
mais aux peuples qui luttent pour la démocratie et la liberté.

Nous ne devons pas rester silencieux face à ceux qui ont assassiné les
camarades Sara [Sakine Cansiz] et Evin. »

ANF


Article de serhildan
=======================

- https://serhildan.org/evin-goyi-mir-perwer-abdurrahman-kizil-trois-portraits/

Heval Evîn est née en 1974 dans le village de Hilal, dans le district de
Qilaban de Şirnex [Shirnak], près du mont Herekol.
Comme lui, elle était majestueuse et rebelle. Le réveil de la société
kurde, provoqué par l’action du 15 août menée par Mahsum Korkmaz marqua
également le début d’une nouvelle vie pour Evîn.

Enfant, Evîn est exposée aux incendies de villages par l’État turc, à
l’oppression et à la torture. Après cela, elle décide de se battre pour
la liberté de son peuple. Elle se rend dans les montagnes en 1988.

Elle développe l’intime conviction que la liberté de la société kurde
sera le résultat de la libération des femmes. Elle combat le colonialisme
turc dans tous les villages de Botan et de Zagros.

Pendant 34 ans, de Herekol à Raqqa, de Shengal à Paris, elle mène une
lutte acharnée et sans répit, une lutte glorieuse.

Evîn Goyî est blessée lors de la guerre contre Daesh au Rojava. Elle vient
alors en France pour se faire soigner.
Là, sa demande d’asile lui est refusée à cause de sa participation à la
lutte armée au Kurdistan.

« Pendant 34 ans, de Herekol à Raqqa, de Shengal à Paris, elle mène une
lutte acharnée et sans répit, une lutte glorieuse. »

Ayant grandi avec la culture de Botan, heval Evîn était une femme courageuse,
travailleuse, consciente de la terre et de la liberté, une pionnière
connue pour ses caractéristiques fortes.

Elle a défié les colonisateurs et les fascistes avec son cœur et sa
conscience. Elle a consacré sa vie à la liberté. Elle a marché dans
les traces de Sakine Cansiz et des femmes martyres du Botan.


Article de Libération du 26 décembre 2022
===========================================

- https://www.liberation.fr/societe/police-justice/attaque-raciste-contre-les-kurdes-emine-kara-a-protege-la-france-la-france-ne-la-pas-protegee-20221226_ZGQTQOHM35DBRDNMONOADKY3QA/

Combattante du Rojava installée en France depuis 2020, Emine Kara, dont
on voit ici le portrait à Paris, le 24 décembre, avait fui la Turquie
en 1994. (Stéphane Lagoutte/Myop pour Libération)

.. figure:: images/emine_kara_5.jpg
   :align: center


«Elle avait combattu Daech les armes à la main, et est morte à Paris, sous
les balles d’un raciste !» : le cri du cœur de Camille, jeune Franco-Kurde
rencontrée samedi 24 décembre place de la République, résume le sentiment
des membres de cette communauté endeuillée.
Car Emine Kara, 48 ans, également connue sous le nom de guerre d’Evin Goyi,
était une « héroïne » de la cause kurde. Son aura de combattante du Rojava,
le Kurdistan syrien, dépassait même dans la jeune génération celle de
Sakine Cansiz, la cofondatrice du Parti des travailleurs du Kurdistan (PKK)
assassinée il y a dix ans à Paris, assure Camille.

Emine Kara avait aussi un rôle de premier plan dans les instances kurdes,
ce qui en faisait une cible de choix, aux yeux du Conseil démocratique kurde
de France (CDKF) : vivant depuis deux ans en France, où elle était venue se
faire soigner après avoir été blessée sur le front à Raqqa, elle présidait
le Mouvement des femmes kurdes, en France et à l’international.
Cette figure bien connue de la diaspora kurde en Europe devait prendre
part vendredi à une réunion de préparation du dixième anniversaire de
:ref:`l’assassinat des trois militantes kurdes <assassinats_2013_01_09>`
rue Lafayette, reportée au dernier moment à cause d’un problème de transports,
selon le CDKF

Du camp de réfugiés de Zakho aux combats contre Daech
----------------------------------------------------------

Difficile, cependant, de faire la part entre la légende entourant Evin Goyi
et la réalité biographique.
Joint par Libération, son avocat, Jean-Louis Malterre, indique, au sujet
de son passé glorieux de partisane, qu’elle «n’en a jamais fait un argument
devant l’Ofpra», l’Office français de protection des réfugiés et apatrides,
auprès de qui elle avait demandé le statut de réfugié politique.

Mais « l’Ofpra a tendance à rejeter les demandes d’asile des combattants kurdes.
Ils vont même jusqu’à demander le retrait du statut quand les réfugiés
ont été condamnés pour être membres du PKK », regrette l’avocat.

Contrairement à Mir Perwer, l’une des trois victimes, Emine Kara, qui se
déclarait enseignante, avait vu sa demande rejetée en février, un refus
confirmé par la Cour nationale du droit d’asile le 26 août 2022.
«La cour a estimé qu’elle n’avait pas une visibilité suffisante pour
justifier son ciblage actuel par les autorités turques», a précisé Me Malterre,
qui avait formé un pourvoi en cassation.

Autrement dit, « comme elle avait quitté la Turquie il y a trente ans,
il n’y avait aucune raison que les autorités turques sachent ce qu’elle
avait fait en Irak et au Rojava ».
Originaire d’Hilal, dans la province de Sirnak, proche de la frontière
irakienne, la quatrième d’une fratrie de cinq fuit la Turquie en mai 1994
après l’incendie et le massacre des hommes de son village.
Dans ces années-là, la guerre entre l’Etat turc et le PKK fait rage,
provoquant la mort de 40 000 personnes. La famille Kara passe d’abord quatre
ans dans le camp de réfugiés de Zakho avant de s’installer dans celui
de Makhmour, situé à 60 km au sud d’Erbil, dans le nord de l’Irak.

En 2014, date de l’instauration d’un califat dans le Nord de l’Irak par
le groupe jihadiste Etat islamique (EI), Emine Kara rejoint le Rojava,
région autonome défendue par les Unités de protection du peuple (YPG),
ces forces armées dont les combattantes ont fait front à la fois contre
l’EI et l’armée syrienne.
En 2019, après la chute de l’EI, elle retourne en Irak mais, blessée,
cherche à gagner l’Europe pour y être soignée. Elle pose ses valises à
Paris, puis « repart en Allemagne pour se faire opérer, avant de revenir
officiellement en France en septembre 2020 », témoigne Me Malterre.

A sa connaissance, ses trois sœurs et son frère résident toujours à
Makhmour. «Le sentiment des Kurdes, c’est qu’elle a protégé la France,
mais que la France ne l’a pas protégée», résume Camille.


