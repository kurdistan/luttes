.. index::
   ! Abdurrahman Kizil

.. _abdullahman_kizil:
.. _abdurrahman_kizil:

=============================================================================
**Abdurrahman Kizil, un welatparêz du Kurdistan #AbdurrahmanKizil**
=============================================================================

- https://serhildan.org/evin-goyi-mir-perwer-abdurrahman-kizil-trois-portraits/


#ParisMassacreOfKurds


.. figure:: images/abdulrahman_kizil.jpg
   :align: center


.. figure:: images/abdurrahman_kizil_2.jpg
   :align: center



Article de Libération du 26 décembre 2022
===========================================

- https://www.liberation.fr/societe/police-justice/attaque-raciste-contre-les-kurdes-emine-kara-a-protege-la-france-la-france-ne-la-pas-protegee-20221226_ZGQTQOHM35DBRDNMONOADKY3QA/


Sur la troisième victime, Abdurrahman Kizil, né en 1963, on sait encore
peu de choses.
Il était « un citoyen kurde ordinaire », un « pilier » de la cafétéria
du centre Ahmet-Kaya où il venait boire le thé et refaire le monde
avec ses compagnons d’exil, selon le CDKF.

Article de serhildan
=======================

- https://serhildan.org/evin-goyi-mir-perwer-abdurrahman-kizil-trois-portraits/

Abdurrahman Kizil a lutté pendant 40 ans.

C’était un :term:`welatparêz` du Kurdistan. Il était l’un des premiers apoïstes
[militant des idées de d’Abdullah Öcalan] de Qaqizman.
Abdurrahman a vécu selon ses convictions, il est tombé martyr dans la
lutte pour la liberté.

Nous publions trois portraits des victimes de l’attaque terroriste du
vendredi 23 décembre à Paris, publiés par Yeni Özgür Politika, traduits
et édités par Serhildan.

Abdurrahman a vécu selon ses convictions
---------------------------------------------

Le militantisme au Kurdistan, en tant que force organisée suivant la
tradition de résistance populaire qui a façonné l’histoire kurde moderne,
a créé des milliers de pionniers au sein du peuple.
Ces welatparêz [2], influencés par les idées du leader du peuple kurde
Abdullah Öcalan, ont pris part à des processus déterminant le destin dans
chaque région du Kurdistan et ont assumé des tâches historiques.

C’est contre eux que s’est dirigée l’oppression la plus implacable du
colonialisme turc. Abdurrahman Kizil était un représentant de cette
tradition, qui a continué à lutter malgré les meurtres, l’exil et
l’emprisonnement.

Abdurrahman Kızıl a lutté sans relâche pendant 40 ans.

Dès qu’il a rencontré le Mouvement kurde à Qaqizman, il a rejoint sans
hésiter la marche pour la liberté. En raison de l’oppression de l’État turc,
il a été contraint de quitter le Kurdistan et s’est rendu d’abord à
Istanbul, puis en Europe. Il était à l’avant-garde de chaque action,
prêt pour chaque tâche.

Parmi les premiers apoïstes
-----------------------------------

Abdurrahman Kizil naît dans le village de Yukari Karagüney, dans le district
de Qaqizman, à Qers. Dans les années 80, lorsque le mouvement pour la
libération des Kurdes prend racine au Kurdistan, il vit ses années de
jeunesse dans la lutte. Il est influencé par ce combat pour la liberté
et y est reste indéfectiblement attaché. Il est alors l’un des premiers
apoïstes de Qaqizman et des dizaines de jeunes avec lesquels il travaille
prennent le chemin des montagnes et de la guérilla.


« Abdurrahman Kizil était un représentant de cette tradition, qui a
continué à lutter malgré les meurtres, l’exil et l’emprisonnement. »

Il subit de plein fouet les politiques d’oppression, de déplacement et
d’incendie de villages menées par l’État turc. En 1989, il est contraint
d’émigrer de ses terres.

La lutte dans les partis
-------------------------------

Abdurrahman Kızıl se rend à Istanbul et reprend là-bas son combat.

Il travaille activement au sein du DEP [Parti de la démocratie, parti
kurde légal depuis interdit et reformé sous un nouveau nom] puis du
HADEP [idem].

Abdurrahman cherche à retourner dans son village depuis Istanbul.
Cependant, à chaque fois, il est détenu et soumis à des tortures intenses.
Les pressions et les détentions se multipliant il est contraint de quitter son pays.

Depuis 2001, il vivait en France, et n’a jamais perdu un seul instant
sa loyauté envers sa culture.

Il était un welatparêz du Kurdistan et considérait que pour cela il
devait travailler à jamais pour son peuple. Il a vécu en accord avec
ces convictions et est tombé martyr dans la lutte pour la liberté.




