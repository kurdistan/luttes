.. index::
   pair: Nagihan ; Akarsel
   ! Nagihan Akarsel
   ! Jineoloji


.. _nagihan_akarsel_2022_10_04:
.. _nagihan_akarsel:

==============================================================================================================================
**2022-10-04 Assassinat de Nagihan Akarsel  l'une des autrices du slogan JinJiyanAzadî (kurdistan Irak) #NagihanAkarsel**
==============================================================================================================================

- :ref:`jin_jiyan_azadi_ref`
- https://www.youtube.com/watch?v=sfCsBOUhs7I (Endama akademî ya jineolojî şehîd Nagîhan Akarsel bi rêzdarî bi bîrtînin)

.. figure:: images/nagihan_akarsel_jin_jyan_azadi.png
   :align: center


.. figure:: images/sulaymaniyah.png
   :align: center

   https://en.wikipedia.org/wiki/Sulaymaniyah


Son assassin est Ismail Peker, un homme turc né à Ankara
==============================================================

Le jeudi 29 décembre 2022, le nom de l’assassin de Nagihan Akarsel
(assassinée de 11 balles dans le cord le corps le 4 octobre 2022 a
Souleimani au Kurdistan du Sud - Irak) et son lien avec la Turquie a été
révélé par **Zubeyir Aydar**, membre du KNK (Conseil National du Kurdistan).

Il s’agit de **Ismail Peker**, un homme turc né à Ankara.

Aydar a déclaré que celui-ci avait confessé avoir été payé et envoyé pour
assassiner Nagihan Akarsel, militante de la Jineolojî et du mouvement des femmes kurdes.

.. _jineoloji:

Jineolojî
============

- https://jineoloji.org/en/2022/10/04/to-the-press-and-public/
- https://jineoloji.org/en/

**Our friend Nagihan Akarsel, Jineolojî Academy member and Jineolojî Journal
Editorial Board member was martyred as a result of an armed attack in
Sulaymaniyah at 9.30 this morning**.

With anger and rage, we condemn this assassination, which is **one of the
recent political murders carried out by fascist and hegemonic powers
against all women in resistance around the world**.

We call on the Kurdistan Regional Government to find the perpetrators of
this murder as soon as possible.

This attack is a continuation of the brutal murders against Yasin Bulut,
Mehmet Zeki Çelebi and Suhel Xorşid. **If this murder case is not investigated,
the collaboration of the Kurdistan Regional Government will face the
rage of the rising wave of our women’s revolution**.

Nagihan has been active in the university youth movement, in the press
and in Jineolojî efforts.

She spent a long time in the Kurdistan Women’s Freedom struggle, and
resisted in prison for years for this cause. During the most difficult
years of the Rojava women’s revolution, she built efforts to develop
the women’s colors of the revolution and advanced revolutionary women’s
enlightenment works through Jineolojî.

She did so in all of **Rojava and especially in Afrin**, with great courage
and determination.

**She touched the souls and hearts of women** in Şengal (Sinjar), who
experienced the most severe forms of ISIS brutality, when she conducted
field research to illuminate this sociology and history.

Our Jineolojî Academy Member Nagihan Akarsel was working on the Kurdish
Women’s Library and Research Center in Sulaymaniyah together with women
from Southern Kurdistan.

While working with the women of Southern Kurdistan, who experienced the
most severe consequences of fascism and nation-state policies, Nagihan
was brutally murdered by the Turkish occupying forces.

We will forever remember Nagihan Akarsel, who has been working for decades
to create the mental and intellectual power of the women’s revolution,
whose slogan **Jin-Jiyan-Azadî** echoes around the world today.

Against the same mentality of patriarchal fascism that brutally murdered
Jîna Aminî, we are growing the women’s revolution in all of Kurdistan
and beyond.

We state that we will avenge our comrade Nagihan by advancing women’s
enlightenment with Jineolojî, in spite of the darkness of fascism and
dominant masculinity.

We call on all revolutionary, democratic, liberationist women, academics
and intellectuals to condemn this political assassination.

We call on Shahnaz Ibrahim and Kafia Suleiman, and all women from Southern
Kurdistan to pressure the Kurdistan Regional Government to illuminate
this political murder, and to condemn this attack by joining actions to
unite with the women’s revolutionary wave in the making in Rojhilat
(Eastern Kurdistan) with the slogan **Jin-Jiyan-Azadî**.

Jineolojî Academy

October 4, 2022

Tweet de Dilar Dirik
======================

- https://twitter.com/Dlrdrk1/status/1577418465358802944?s=20&t=tMG-lmR5kblogfqB0LMq_w


Jin, jiyan, azadî is not a fluffy feel-good motto. It's a philosophy
developed in a four decades-old anti-colonial movement. It has consequences.

Today, a revolutionary that lived & grew this philosophy was murdered.
**Nagihan Akarsel's assassination must not go unnoticed !**


.. figure:: images/nagihan_akarsel.png
   :align: center

Tweet de starrcongress (2022-10-04)
=======================================

- https://twitter.com/starrcongress/status/1577333069517934593?s=20&t=JucnMynChHVif4BM6mCdcA

The Kurdish women's movement in Europe protested against in Frankfuhrt
the deadly attack on :ref:`#NagihanAkarsel <nagihan_akarsel>` |NagihanAkarsel| in Suleymaniya.

**She was one of the original proponents of the** :ref:`#JinJiyanAzadî <femmes_vie_liberte>` slogan.

#StopFeminicides

Tweet de Cahîda Dêrsim
============================

- https://twitter.com/dersi4m/status/1577225398383149057?s=20&t=Ilt20K6fn0XZOTf5h21t_Q


What a sad news: Kurdish female activist, academician and editor of Jineoji
magazine and member of the Jineology Research Center in Silemani #NagihanAkarsel
has been shot dead in front of her house this morning in the city Silemani
(@kurmancirojnews) #TwitterKurds #JinJiyanAzadi

#Jineology (Jineolojî), translated as "science of women," is a central
component of the #Kurdish liberation movement and plays a major role at
the institutional level, especially in the #Rojava revolution #TwitterKurds


#Jineology views women's individual freedom as an indispensable prerequisite
for the freedom of society as a whole and focuses on the study of society,
history, religion, epistemology and many other areas from a woman's
perspective #NagihanAkarsel


Our assassinated friend Nagihan Akarsel on the shoulders of resisting women.... 🌹
=======================================================================================

- https://twitter.com/Dlrdrk1/status/1578713608913313792?s=20&t=naPos8Uso623H1lvtXTuKw

May your light illuminate the path of the women's liberation struggle☀️


An elegy for Nagihan Akarsel
===============================

- https://medyanews.net/an-elegy-for-nagihan-akarsel/


Nagihan Akarsel, a Kurdish scholar, activist, journalist and co-founder
of the feminist Jineology Research Centre, was shot dead earlier this
week in an armed attack in Sulaymaniyah, Iraqi Kurdistan.

Her death, which recalls previous extrajudicial killings of Kurdish
women’s activists by the Turkish intelligence services, has sparked
protests in Kurdistan and around the world. Here, her friend and colleague
Haskar Kırmızıgül shares some reflections on her life, work and
untimely death.

The audio version is read by Rahila Gupta.


Femmes, Vie, Liberté : des kurdes montpelliérains dénoncent le meurtre d’une féministe en Irak, en écho à la révolte iranienne
=======================================================================================================================================


- https://lepoing.net/femmes-vie-liberte-des-kurdes-montpellierains-denoncent-le-meurtre-dune-feministe-en-irak-en-echo-a-la-revolte-iranienne/


.. figure:: images/montpellier_nagihan_akarsel.png
   :align: center


Une cinquantaine de kurdes de Montpellier ont montré leur indignation
dans la soirée du 5 octobre devant l’assassinat de la journaliste :ref:`#NagihanAkarsel <nagihan_akarsel>` |NagihanAkarsel|,
éditrice et co-rédactrice en chef du magasine Jineologî et membre du centre
de recherche en jinéologie [NDLR : science de la libération des femmes, au Kurdistan]
au Kurdistan irakien, devant son domicile à Souleymaniye le 4 octobre.

Les kurdes montpelliérains présents sur les marches de l’Opéra Comédie
accusaient lors du rassemblement le régime turc de Recep Tayip Erdogan.

Ce qui paraît très probable : pour le bureau Moyen-Orient de Reporters
sans frontières (RSF), « il s’agit de la cinquième attaque, dont quatre
mortelles, contre un résident d’origine turque au Kurdistan irakien ou
un activiste critique du gouvernement turc en moins d’un an. »

L’hommage à la victime, considérée comme une illustre militante féministe
kurde, a été ponctuée des mots « Jin, Jiyan, Azadi », soit « femmes, vie, liberté »
en kurde, en écho à la grande révolte qui secoue actuellement l’Iran
après la mort d’une femme suite à son arrestation par la police des mœurs
pour non-port du voile à Téhéran.

[NDLR : le slogan tant repris dans les manifestations iraniennes provient
effectivement du mouvement féministe au Kurdistan iranien.]

L’opposition kurde au régime d’Erdogan porte un projet de société
foncièrement progressiste, partiellement mis en œuvre au Rojava,
le Kurdistan syrien, depuis le 17 mars 2016 : auto-détermination du
peuple kurde, cohabitation avec les autres communautés ethniques et
religieuses de la région, exploration profonde d’une voie vers la démocratie
directe, émancipation poussée des femmes envers les carcans patriarcaux,
refondation totale d’institutions traditionnellement à la botte des pouvoirs
comme la police et la justice, et élaboration d’une économie coopérative
tentant de dépasser le capitalisme et de répondre à l’urgence écologique.


La militante écologiste Greta Thunberg a participé à la manifestation de Stockholm condamnant le meurtre de #NagihanAkarsel
=============================================================================================================================

- https://twitter.com/KurdistanAu/status/1577915731127275521?s=20&t=4MqX7Oocn2mDbdcIq_WqLw


.. figure:: images/suede_greta_thunberg.png
   :align: center

SUEDE La militante écologiste Greta Thunberg a participé à la manifestation
de Stockholm condamnant le meurtre de #NagihanAkarsel, journaliste & féministe
kurde assassinée par ds agents turcs (MIT) au Kurdistan d'Irak

#TwitterKurds #MahsaAmini  #JinJiyanAzadi #JinaAmini

Meral Çiçek on Nagihan Akarsel and the origins of Jin, Jiyan, Azadi
=========================================================================

- https://www.youtube.com/watch?v=LoBZtDYLlV8
- https://medyanews.net/meral-cicek-on-nagihan-akarsel-and-the-origins-of-jin-jiyan-azadi/


At times it is hard to keep up with the fast-moving developments in the
Kurdish issue, and particularly in the last three weeks this has been
especially true, with Kurds in Iran taking part in general strikes,
taking to the streets and in some places taking control of towns in
uprisings sparked by the brutal killing of a young 22-year-old Kurdish woman,
Jina Amini, by the so-called ‘morality police’ of the Iranian regime.

In fact women all over Iran, with the support of men, have poured onto
the streets, ripping off their headscarves in rage and protest against
the ‘compulsory hijab’ laws, with the chant, ‘Jin, Jîyan, Azadî’ ‘Woman, Life, Freedom’
in what has become the biggest challenge to the Iranian regime since the
Iranian revolution of 1978/79, its outcome still unknown at this point,
still being played out on the streets of Iran and East Kurdistan (Rojhilat/NW Iran)
as you read this.

The leading chant of the uprisings and protests in East Kurdistan against
the killing of Nagihan Akarsel has been ‘Jin, Jiyan, Azadi’ or ‘Woman, Life, Freedom’,
a well-known slogan of the Rojava (West Kurdistan|) Women’s Revolution
that inspired the defeat of ISIS and no doubt also inspired the women,
certainly of Kurdish NW Iran but also Iranian women more widely.

But what is the origin of ‘Jin, Jiyan, Azadi’? And how have the theories
born and developed in the Kurdish Women’s Freedom Movement as taught
and promoted by Nagihan and her comrades, inspired or developed theories
of women’s emancipation in the Middle East and beyond ?

Well, I’m very honoured and happy to be joined by Meral Çiçek.

Meral was born in 1983 in a Kurdish guest-worker family in Germany.

She started political and women’s activism at the age of 16 within the
Kurdish Women’s Peace Office in Dusseldorf. While studying Political
Science in Frankfurt she started to work as a reporter and editor for
the only daily Kurdish newspaper in Europe, Yeni Özgür Politika, for
which she still writes a weekly column.

In 2014 Meral co-founded the Kurdish Women’s Relations Office in Southern
Kurdistan (Northern Iraq). She is also an editorial board member of the
Jineoloji journal.

On the day of Nagihan’s funeral I invited Meral to share with us a little
bit about Nagihan and her work in South Kurdistan and then asked her
if she could also kindly share with us and explain the origins of the
now legendary slogan, ‘Jin, Jiyan, Azadi’ that is inspiring and arguably
now leading a woman’s revolution in the Middle East, and how it can be
traced back directly to the ideas of the imprisoned Kurdish leader,
Abdullah Öcalan and the Kurdish women’s movement within the Kurdistan
Workers’ Party (PKK).

Meral also explained why she thought the Turkish state was pursuing a
specific* strategy of targeting Kurdish women, what she thought the
legacy was of these women who had been killed by the Turkish state
and how best we can honour them.

Please listen to the podcast for the whole interview.


