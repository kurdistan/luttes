.. index::
   pair: Assassinats ; Vendredi 23 décembre 2022
   pair: Terrorisme ; Vendredi 23 décembre 2022

.. _assassinat_kurdes_2022_12_23:

===========================================================================================================================================
Vendredi 23 décembre 2022 **Attaque terroriste contre le siège du CDK-F à Paris**  #JesuisKurde #Paris10 #ParisAttacks #TwitterKurds
===========================================================================================================================================

- https://cdkf.fr/attaque-terroriste-contre-le-siege-du-cdk-f-a-paris/
- https://blogs.mediapart.fr/carol-mann/blog/241222/silence-tue-encore-des-kurdes-paris

#EmineKara (#EvînGoyî), #MîrPerwer et #AbdurrahmanKızıl,
#JesuisKurde #Paris10 #ParisAttacks #TwitterKurds #AttentatTerroriste #Paris #Defendkurdistan #le2412 #ParisMassacreOfKurds  #Justice4Kurds
#noel #Christmas #paris10 #JeSuisKurde #Justice4Kurds #TwitterKurds #JinJiyanAzadi #ParisAttacks

- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

.. figure:: images/annonce_cdk_fr.png
   :align: center


.. figure:: images/je_suis_kurde_2.jpg
   :align: center
   :width: 400


Victimes de l'attentat terroriste visant les Kurdes à Paris
=============================================================

- 🥀Evîn Goyî (Emine Kara)
- 🥀Mîr Perwer
- 🥀Abdullah Kizil


PARIS. L’ombre des services secrets turcs plane au-dessus du massacre
de la rue d’Enghien Les victimes sont Evîn Goyî (Emine Kara), Mir Perwer
& l’activiste Abdullah Kızıl.


.. figure:: images/les_3_victimes.png
   :align: center

   https://kurdistan-au-feminin.fr/2022/12/24/paris-services-secrets-turcs-massacre-rue-denghien-paris/


Evin Goyi (Emine Kara)
==========================

#JesuisKurde #Paris10 #ParisAttacks #TwitterKurds #AttentatTerroriste #Paris

UNE MILITANTE KURDE TUÉE A PARIS

.. figure:: images/evin_goyi_emine_kara.jpg
   :align: center


Evin Goyi (Emine Kara) était une combattante dans la guerre contre
DAECH / ISIS au Rojava.
Elle était venue à Paris pour des soins et elle a été tuée lors de
l'attentat terroriste d'aujourd'hui.

Emine Kara était la responsable du Mouvement des femmes kurdes en France.

Elle était celle dont la France avait refusé la demande d’asile.

Elle se démenait depuis plusieurs mois pour organiser la solidarité avec la
révolution des femmes en Iran ou encore les mobilisations du 25 novembre.

Source: https://twitter.com/KurdistanAu/

ROJAVA. Kongra Star condamne le massacre de Paris
----------------------------------------------------

- https://kurdistan-au-feminin.fr/2022/12/24/rojava-konmassacre-de-paris-kongra-star/

SYRIE / ROJAVA – Le mouvement des femmes du Rojava, Kongra Star a manifesté
aujourd’hui à Qamishlo pour condamner l’assassinat des militants et
artistes kurdes Emine Kara (Evîn Goyî), Abdurrahman Kızıl, Mîr Perwer
qui a été commis hier dans la rue d’Enghien, à Paris


Mîran Perwer
==============

- https://kurdistan-au-feminin.fr/2022/12/24/massacre-paris-soutenir-les-artistes-kurdes/

La deuxième victime est Mîran Perwer, un chanteur bien connu dans la
communauté, originaire de Muş-Varto au Bakur (Kurdistan du Nord).

Il avait du fuir la Turquie où il était poursuivi pour ses activités
politiques et notamment son soutien au HDP.



Attaque terroriste contre le siège du CDK-F à Paris
=======================================================

- https://cdkf.fr/attaque-terroriste-contre-le-siege-du-cdk-f-a-paris/


Ce vendredi 23 décembre 2022, en fin de matinée, une attaque terroriste
a été perpétrée par un homme armé, contre notre siège, au 16 rue d’Enghien,
dans le 10e arrondissement de Paris.

Pour le moment, on dénombre au moins trois militants kurdes décédés et
plusieurs blessés, dont trois dans un état grave.

Le Conseil démocratique kurde en France (CDK-F) condamne avec virulence
cette attaque terroriste infâme qui intervient suite à de multiples menaces
proférées par la Turquie, alliée de Daesh. 

Cet attentat terroriste survient également peu avant le 10e anniversaire
du triple  assassinat des militantes kurdes à Paris le 9 janvier 2013,
des meurtres qui restent toujours impunis en raison du refus de la France
de lever le secret-défense. 

Nous tiendrons une conférence de presse ce soir, à 18h30, dans nos locaux. 

Nous appelons tout le monde à dénoncer cette attaque ignoble en se
rassemblant dès à présent devant 16 rue d’Enghien.

Par ailleurs, nous vous appelons à participer à une veille nocturne qui
aura lieu cette nuit, dans nos locaux, pour protester contre cet attentat
terroriste et rendre hommage aux militants kurdes assassinés.

Conférence de presse avec David Andic (avocat du CDKF), Agit Polat (représentant du CDKF) et Berivan Firat (porte-parole des relations extérieures du CDKF)
===============================================================================================================================================================

- https://invidious.baczek.me/watch?v=1GOXcE-Isx4
- https://framapiaf.s3.framasoft.org/framapiaf/cache/media_attachments/files/109/565/424/598/475/080/original/50de02a68b4e37fc.mp4

Conférence de presse avec David Andic (avocat du CDKF), Agit Polat
(représentant du CDKF) et Berivan Firat (représentante du CDKF) avec
une intrvention de Jean-Luc Mélenchon.

Communiqué de AIAK
=====================

Attaque contre le CDKF

Bonjour,

AIAK apporte ses plus sincères condoléances aux victimes et à leurs proches
et demande à ce que justice soit faite face à ce terrible attentat.

Nous appelons à un rassemblement ce samedi 24 décembre 2022 à 14h00
rue Felix Poulat 3800 Grenoble

Ce rassemblement marquera la volonté de celles et ceux qui y participeront :

- de rendre hommage aux victimes de l’attentat de Paris le 23 décembre 2022
- d'apporter notre solidarité avec la communauté Kurde
- de demander aux autorités françaises de renforcer la protection de la
  communauté kurde et de combattre les agissements violents, notamment
  encouragées par les autorités gouvernementales turques

Pour aiak Maryvonne Matheoud et Ali Arslan co-présidents



La responsable du Mouvement des Femmes kurdes en France se trouve parmi les victimes du massacre de Paris
==============================================================================================================

- https://kurdistan-au-feminin.fr/2022/12/23/massacre-de-paris-jinjiyanazadi-parisattacks/

La responsable du Mouvement des Femmes kurdes en France se trouve parmi
les victimes du massacre de Paris qui a eu lieu aujourd’hui dans le 10e
arrondissement de Paris. La nouvelle a été annoncée par le Conseil
démocratique kurde en France (CDK-F)

.. figure:: images/mouvement_des_femmes_kurdes_en_france.png
   :align: center

   https://kurdistan-au-feminin.fr/2022/12/23/massacre-de-paris-jinjiyanazadi-parisattacks/



Soutien de Anne Hidalgo
============================

- https://nitter.kavin.rocks/Anne_Hidalgo/status/1606286545731743744#m

La communauté kurde et, à travers elle tous les Parisiens, a été visée
par ces assassinats commis par un militant d’extrême-droite.
Les Kurdes où qu’ils résident doivent pouvoir vivre en paix et en
sécurité. Plus que jamais, Paris est à leurs côtés dans ces heures sombres.



Une réunion d’une soixantaine de militantes kurdes était prévue
===================================================================

Difficile de croire au hasard. Une soixantaine de militantes kurdes
devaient être là, sur ces mêmes marches, ce vendredi, lorsque l’assassin
a fait feu sur les quelques personnes présentes.

L’assassin connaissait-il la date et l’heure de la réunion ? Il ne savait
pas que celle-ci avait été décalée dans la matinée à cause de problèmes
de RER et ne devait démarrer qu’une heure plus tard.
Un massacre a été évité, bien qu’il se soit ensuite tourné vers le
restaurant kurde, situé de l’autre côté de la rue puis vers un salon de
coiffure attenant.

Le centre kurde n’était pas protégé par la police
====================================================

Berivan Firat, porte-parole des relations extérieures du CDKF ne retient
pas sa colère ni sa tristesse. « Nous sommes à la veille du 9 janvier.
Il y a dix ans trois femmes Kurdes étaient assassinées. C’est encore une
attaque visant des femmes.
Qu’on ne nous dise pas qu’il s’agit seulement d’un acte raciste.

Si les commanditaires des meurtres du 9 janvier 2013 avaient été démasqués
(seul le tueur a été identifié mais aujourd’hui décédé, N.D.L.R.) ,
la tuerie d’aujourd’hui ne se serait pas produite. »

Malheureusement, malgré les demandes de la juge d’instruction chargée
du dossier, le secret-défense n’a jamais été levé par la France.
Or, les relations entre les services de renseignements turcs, le MIT,
et français, la DGSI, sont au beau fixe. Comme le fait remarquer la
porte-parole des relations extérieures du CDKF, « on criminalise les
Kurdes au lieu de les protéger et on encourage Erdogan ».

De fait, le centre du CDKF n’était pas sous surveillance policière
malgré les menaces venant de toutes parts.

Le meurtrier de Fidan, Sakile et Leyla avait été fiché par la DGSI en
janvier 2012 soit un an avant les assassinats sans que cela n’éveille
la curiosité des autorités françaises.

Celles-ci ont pourtant « le devoir moral de protéger les Kurdes qui ont
versé leur sang pour battre l’État islamique à Kobané », souligne Berivan Firat.


Appel à la manifestation
=============================

- https://paris-luttes.info/manifestation-contre-l-attaque-du-16501

Vendredi 23 décembre, un homme connu des forces de police a perpétré une
attaque à l'arme à feu contre le Centre culturel kurde Ahmed Kaya tuant
deux personnes sur le coup, une mourrant à la suite de ses blessures
et en blessant trois autres.

Rassemblons-nous à Place de la République à 12:00 pour demander justice
pour le peuple kurde !

Vendredi 23 décembre, un homme connu des forces de police a perpétré une
attaque à l'arme à feu contre le Centre culturel kurde Ahmed Kaya tuant
deux personnes sur la coup, une mourrant à la suite de ses blessures
et en blessant trois autres.

Cet attentat de nature raciste et fasciste n'est pas un hasard, est
spécifiquement tourné contre le peuple kurde.

Selon les informations de l'Humanité, l'homme aurait été déposé en voiture
devant le centre où devait se ternir une réunion pour l'organisation de
la manifestation du 7 janvier, à la mémoire du triple assassinat des
militantes kurdes survenu à Paris il y a bientôt 10 ans.

Heureusement, la réunion a été retardée d'une heure, évitant un massacre.

C'est un crime organisé spécifiquement contre le peuple kurde.

Face à ce crime odieu, rassemblons-nous à Place de la République à 12:00
pour demander justice pour le peuple kurde !

Nous ne lacherons rien ! nous demandons justice pour Sakine Cansiz,
Fidan Dogan et Leyla Saylemez, pour nos trois camarades tué-e-s aujourd'hui.

Biji PKK !
Biji Kurdistan !
Jin, Jiyan, Azadî !

Rendez-vous à 12:00 à Place de la République.


PKK calls on Kurds & their friends to avoid acts of violence
===============================================================

- https://twitter.com/KurdistanAu/status/1606425824940511233#m

PKK calls on Kurds & their friends to avoid acts of violence while expressing
their reactions in a democratic way, & warns them against provocations.

#parisattack #Paris10 #TwitterKurds #jesuiskurde


PARIS. L’ombre des services secrets turcs plane au-dessus du massacre de la rue d’Enghien

Les victimes sont Evîn Goyî (Emine Kara), Mir Perwer & l’activiste Abdullah Kızıl.

#parisattack #Paris10 #TwitterKurds #jesuiskurde

