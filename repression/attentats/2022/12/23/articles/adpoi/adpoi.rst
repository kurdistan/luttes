
.. _communique_adpoi_2022_12_24:

==================================================================================================
Communiqué de l'ADPOI (Association de Défense des Prisonniers Politiques et d’Opinion en Iran)
==================================================================================================

Nous Condamnons l’attaque raciste contre le centre culturel Kurde à Paris
et l’Assassinat de 3 militant.e.s Kurdes

Le vendredi 23 décembre 2022, un individu de 69 ans avec le passé raciste
a attaqué le centre culturel Kurde et a assassiné 3 militants dont une
femme, Eminé KARA.
Elle avait milité dans les 4 parties du Kurdistan en Turquie, Irak, Syrie
et l’Iran.

L’agresseur avait déjà attaqué, il y a un an, un centre des immigrés en
blessant 3 personnes mais avait été libéré il y a deux semaines sans
aucun jugement.

Rappelons que l’assassinat de 3 militantes kurdes en janvier 2013 n’a
pas encore été élucidé.
Les kurdes résidents en France demandent toujours la justice.

Nous condamnons vivement cette attaque et ces assassinats et exigeons du
gouvernement français une meilleure protection des réfugiés politiques
kurdes en France qui vivent, en particulier sous la menace du gouvernement
turc.

Nous sommes solidaires avec les blessés de l’attaque, les familles des
victimes et les militants kurdes en lutte.

Association de Défense des Prisonniers Politiques et d’Opinion en Iran (ADPOI)- Paris10
A.D.P.O.I. 19, Avenue d’Italie 75013 Paris
(Association régie par la Loi du 1er juillet 1901 - J.O. de 9 août 1995



