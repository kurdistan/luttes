
.. _articles_humanite_2022_12_26:

=======================================
2022-12-23 Articles de l'Humanité
=======================================



Crime raciste ou acte terroriste anti-Kurdes ?
==================================================

:download:`attentat_terroriste_paris.pdf`


RUE D’ENGHIEN L’attaque meurtrière de vendredi à Paris est-elle le fait
d’un détraqué xénophobe ? Pour les Kurdes,

Cet attentat rappelle celui qui a tué trois militantes en 2013.


Le-tueur-de-la-rue-d-enghien-en-mission-contre-les-militants-kurdes
=====================================================================

- https://www.humanite.fr/politique/kurdes/le-tueur-de-la-rue-d-enghien-en-mission-contre-les-militants-kurdes-775961?s=04

Selon les informations recueillies par l’Humanité, l’homme aurait été
déposé par une voiture devant le siège du Conseil démocratique kurde de
France (CDKF) alors que devait se tenir une réunion d’une soixantaine
de femmes kurdes, finalement décalée d’une heure au dernier moment.

Un massacre a été évité. Qui aurait renseigné le tueur ?

Or, de nombreux éléments laissent penser qu’il pourrait s’agir d’une
attaque non pas dirigée contre de simples étrangers, mais au contraire
d’un acte politique visant explicitement non seulement les Kurdes mais
plus directement le Conseil démocratique kurde de France (CDKF).

Selon les éléments que nous avons pu recueillir, une voiture aurait
déposé William G. devant le siège du CDKF. Il est descendu d’un véhicule,
son arme à la main, à l’heure même où devait se tenir une importante
réunion de femmes kurdes pour préparer les manifestations du 4 et du 7 janvier,
commémorant le 10e anniversaire de l’assassinat de trois militantes
kurdes (Fidan Dogan, Sakile Cansiz et Leyla Soylemez) en plein Paris,
le 9 janvier 2013, et dont les commanditaires ne sont toujours pas identifiés.

