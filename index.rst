
.. raw:: html

   <a rel="me" href="https://kolektiva.social/@iranluttes"></a>
   <a rel="me" href="https://kolektiva.social/@kurdistanluttes"></a>


|FluxWeb| `RSS <https://kurdistan.frama.io/luttes/rss.xml>`_

.. _kurdistan_luttes:
.. _luttes_kurdistan:

==========================
**Luttes au Kurdistan**
==========================

.. figure:: images/mahsa_jina_amini_avatar.png
   :align: center

   https://fr.wikipedia.org/wiki/Kurdistan

.. toctree::
   :maxdepth: 7

   actions/actions
   evenements/evenements
   organisations/organisations
   repression/repression
   militantes/militantes


.. toctree::
   :maxdepth: 3

   slogans/slogans
   media/media
   glossaire/glossaire

