.. index::
   pair: Documentaire; notturno
   ! notturno


.. _notturno:

=====================
notturno
=====================

- https://www.on-tenk.com/fr/documentaires/societe/notturno

.. figure:: images/notturno.png
   :align: center

Résumé
=========

De combien de douleurs, de combien de vies se compose l’existence au
Moyen-Orient ?

Notturno a été tourné pendant trois ans le long des frontières de l’Irak,
du Kurdistan, de la Syrie et du Liban ; tout autour, des signes de violence
et de destruction, et au premier plan l’humanité qui se réveille chaque
jour d’une nuit qui paraît infinie.


L'avis de Tënk
=================

Il n’y a pas besoin de connaître les tenants et les aboutissants des
conflits du Moyen-Orient pour comprendre ce que Gianfranco Rosi veut
nous dire de la douleur des gens.

Les compositions picturales et l’ambiance feutrée de Notturno ne voilent
pas la réalité : la pudeur est un luxe que le réalisateur ne peut pas
se permettre quand il raconte le malheur qui broie les âmes et les corps
des populations du Moyen-Orient.

C’est un film de peu de mots, car qui a encore la force de parler en ces
terres, le courage de dire la peine quand elle est si grande.

Il cherche et trouve son cœur à deux endroits, dans les dessins d’enfants
qui racontent les crimes de guerre qu’ils ont subis et dans la salle de
théâtre d’un asile, où les patient·es répètent les discours qui font et
défont le Moyen-Orient.

Un film comme une longue aube, où le jour peine à conquérir la nuit.

Baume Moinet-Marillaud
Chargé de diffusion audiovisuelle à Tënk


Né à Asmara, en Erythrée, citoyen italien et américain, Gianfranco Rosi
a étudié le cinéma à l'Université de New York.

Il est l’auteur de plusieurs documentaires remarqués.

Below Sea Level, son premier long métrage, a notamment remporté le Grand
Prix du festival Cinéma du Réel.

Ses films Sacro GRA, sur le périphérique de Rome, et Fuocoammare sur
Lampedusa, sont les premiers documentaires à obtenir respectivement un
Lion d’Or au Festival International du film de Venise et un Ours d'Or à
la Berlinale.

En 2020 son film Notturno a été sélectionné à la Mostra de Venise et il
est sorti en salle en France en septembre 2021.


