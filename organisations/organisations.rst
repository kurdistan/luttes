
.. _soutiens_kurdistan:

=======================================
Organisations
=======================================

- https://serhildan.org/
- https://www.instagram.com/lutteskurdes/

.. toctree::
   :maxdepth: 5

   aiak/aiak
   cdkf_fr/cdk_fr
   cnsk/cnsk
   kck/kck   
   serhildan/serhildan
   tjke/tjke
