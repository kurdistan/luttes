.. index::
   ! CNSK
   ! Coordination nationale Solidarité Kurdistan

.. _cnsk:

=========================================================
Coordination nationale Solidarité Kurdistan (CNSK)
=========================================================

Coordination Nationale Solidarité Kurdistan::

    - 16, rue d’Enghien 75010 Paris
    - Tél : +33 6 45 41 76 68
    - Email : cnskurd@gmail.com

- Amis du Peuple Kurde en Alsace
- Amitiés Corse Kurdistan
- Amitiés Kurdes de Bretagne (`AKB <https://www.akb.bzh/>`)
- Amitiés Kurdes de Lyon Rhône Alpes
- Association Iséroise des Amis des Kurdes (`AIAK <aiak>`)
- Association Solidarité France Kurdistan
- Centre d’Information du Kurdistan (CIK)
- Collectif Azadi Kurdistan Vendée (CAKV)
- Conseil Démocratique Kurde de France (CDK-F)
- Ensemble
- Mouvement de la Jeunesse Communiste de France
- Mouvement de la Paix
- Mouvement des Femmes Kurdes en France (TJK-F)
- MRAP (Mouvement contre le Racisme et pour l’Amitié́ entre les Peuples)
- Nouveau Parti Anticapitaliste (NPA)
- Parti Communiste Français (PCF)
- Réseau Sortir du Colonialisme
- Union Communiste Libertaire
- Union Démocratique Bretonne (UDB))
- Union Syndicale Solidaire
- Solidarité́ et Liberté́ Provence.
