.. index::
   ! KCK
   ! Coordination nationale Solidarité Kurdistan

.. _kck:

=========================================================
KCK **Kurdistan Democratic Communities Union**
=========================================================

- https://kck-info.com
- https://mastodon.social/@KCK_Kurdistan_
