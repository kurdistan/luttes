.. index::
   ! cdk-fr


.. _cdk_fr:

==================================================================================================================
|cdkf_fr| **Conseil Démocratique Kurde en France** (CDK-FR)
==================================================================================================================

- https://cdkf.fr/


.. figure:: images/cdkf_fr.png
   :align: center


.. toctree::
   :maxdepth: 3

   a-propos/a-propos
   membres/membres
