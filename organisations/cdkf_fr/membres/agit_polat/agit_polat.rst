.. index::
   ! Agit Polat

.. _agit_polat:

==================================================================================================================
**Agit Polat** Porte-parole du Conseil Démocratique kurde en France (CDK-F)
==================================================================================================================

- https://nitter.kavin.rocks/Ag_plt1

.. figure:: images/agit_polat_0.png
   :align: center



.. figure:: images/agit_polat_1.png
   :align: center
