.. index::
   pair: Berivan Firat ; Réunion publique **Histoire de la résistance kurde (2025-01-25)

===============================================================================================================================================
2025-01-26 Réunion publique **Histoire de la résistance kurde** avec Berivan Firat dimanche 26 janvier 2025 17h30 à Saint-Martin d'Hères
===============================================================================================================================================


- :ref:`aiak:berivan_firat_2025_01_26`
