.. index::
   pair: Berivan ; Firat
   ! Berivan Firat

.. _berivan_firat:

==================================================================================================================
**Berivan Firat** Porte-parole des relations extérieures du Conseil Démocratique kurde en France (CDK-F)
==================================================================================================================

- http://bird.trom.tf/Berivanfirat75

.. figure:: images/berivan_firat_0.jpg
   :align: center

   http://bird.trom.tf/Berivanfirat75


.. figure:: images/berivan_firat_1.jpg
   :align: center


.. toctree::
   :maxdepth: 5

   2025/2025
   2022/2022
