
.. _cdk_fr_a_propos:

==================================================================================================================
Qu’est-ce que le Conseil démocratique kurde en France ?
==================================================================================================================

- https://cdkf.fr/

.. figure:: ../../../images/cdkf_fr_logo.png
   :align: center

Le Conseil démocratique kurde en France (CDK-F) est une structure qui
regroupe et fédère 24 associations de la diaspora kurde à travers toute la France.

Anciennement Fédération des Associations kurdes en France (FEYKA), le
CDK-F a pris son nom actuel en 2014, suite à une modification en profondeur
de ses statuts destinée à introduire un système de démocratie participative
dans l’ensemble de son organisation et de son fonctionnement.

Comme le prévoient ses statuts, le CDK-F a pour mission d’aider au
rapprochement des membres de la communauté kurde, de sauvegarder et
d’améliorer la langue de celle-ci, son identité et ses valeurs, de
défendre ses libertés, ses droits sociaux, culturels, économiques et politiques,
tout en respectant ceux des autres communautés et les valeurs universelles, …

Depuis sa création en 1994, le CDKF œuvre pour faire connaître les Kurdes,
leur culture, leur langue, leur situation politique dans chacun des pays
qui se divisent le Kurdistan.

Principale organisation représentative des Kurdes en France, il mène des
actions au niveau national pour la reconnaissance des droits légitimes
du peuple kurde et pour dénoncer toutes les violations dont celui-ci
est victime.

A ce titre, il joue un rôle de représentation auprès des autorités
politiques françaises. Sur le plan politique, il agit en particulier
pour faire connaître et promouvoir le projet de confédéralisme démocratique
porté par le mouvement kurde dans les quatre parties du Kurdistan et
actuellement mis en œuvre au Rojava (Ouest-Kurdistan, Syrie).

Ce projet développé par le leader du mouvement kurde Abdullah Öcalan,
détenu en Turquie depuis 1999, propose un système d’autogouvernance qui
permettrait d’apporter une solution à la question kurde sans toucher aux
frontières.

Fondé sur la démocratie participative, l’écologie et l’égalité des sexes,
ce projet vise plus largement à résoudre les conflits ethniques et
confessionnels dans l’ensemble du Moyen-Orient.

Le CDK-F aspire par ailleurs à être un pont entre les Kurdes et toutes
les autres communautés vivant en France. Les différentes associations
qui en sont membres sont ouvertes à toute personne, quelle que soit son
appartenance ethnique, sa religion ou son identité sexuelle, sous
réserve de respect des valeurs qui les fondent, telles la démocratie,
l’éthique, l’égalité hommes-femmes, la tolérance.

Le CDKF agit partout en France pour promouvoir le dialogue interculturel
et pour faire avancer la solidarité entre les peuples.

Le CDKF, c’est aussi des centaines de bénévoles qui se mobilisent partout
en France pour la réalisation de ses objectifs.
Des cours de langue kurde à l’organisation des manifestations culturelles
ou politiques, en passant par la gestion administrative, le travail des
associations kurdes repose quasi-exclusivement sur les efforts de bénévoles
motivés et convaincus.

Il est également important de souligner que le CDK-F travaille en étroite
coopération avec diverses organisations françaises au sein d’un réseau
appelé Coordination nationale Solidarité Kurdistan (CNSK) qui joue un
rôle très important d’observation et d’information sur la situation des Kurdes.

Le CDK-F fait par ailleurs partie d’un réseau européen d’associations
kurdes, le Congrès de la Société démocratique du Kurdistan en Europe (KCDK-E).


