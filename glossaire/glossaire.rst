.. index::
   ! Glossaire

.. _glossaire_kurdistan:

=======================================
Glossaire
=======================================

.. glossary::

   welatparêz
       Le terme welatparêz signifie « protecteur du pays », il est souvent
       traduit par « patriote » mais il porte au contraire une dimension
       révolutionnaire et n’est pas nationaliste.

       Il est utilisé en kurde pour désigner les militants civils du PKK.
