


.. _caroline_guibet_lafaye_2025_02_07:

======================================================================================================================================================================================
2025-02-07 L'enquête du CNRS a été menée exclusivement à charge» : la suspension d’une scientifique travaillant sur le PKK provoque la controverse dans le milieu de la recherche
======================================================================================================================================================================================

- https://france3-regions.francetvinfo.fr/occitanie/haute-garonne/toulouse/l-enquete-du-cnrs-a-ete-menee-exclusivement-a-charge-la-suspension-d-une-scientifique-travaillant-sur-le-pkk-provoque-la-controverse-dans-le-milieu-de-la-recherche-3104584.html
- https://lisst.univ-tlse2.fr/
- https://cv.hal.science/caroline-guibet-lafaye



Thèmes de recherche
==========================

Caroline Guibet Lafaye consacre ses recherches en sociologie et en philosophie 
politique à l’analyse des sentiments d’injustice et des processus de radicalisation. 

Elle s’appuie sur un corpus normatif de philosophie et de sociologie empirique 
pour cerner les processus de légitimation de la violence politique ainsi 
que le « vocabulaire de motifs » et les « politiques de la signification », 
convoqués par plusieurs organisations de lutte armée en Europe occidentale 
(extrême gauche, libération nationale) et au Moyen-Orient. 

Elle a publié en 2019 Armes et principes. Éthique de l’engagement politique 
armé (éd. du Croquant) et en 2020 Le conflit au pays basque : regards des 
militants illégaux (Peter Lang).



Auteurs
============

- https://france3-regions.francetvinfo.fr/redaction/xavier-lalu
- https://france3-regions.francetvinfo.fr/redaction/sylvain-duchampt

Préambule
============

Le CNRS a suspendu pour deux ans, dont un avec sursis, Caroline Guibet Lafaye, 
sociologue, spécialiste reconnue de la radicalisation, pour des travaux sur le PKK. 

Cette sanction, la plus lourde en six ans, soulève une controverse sur 
l'anonymisation des sources et les enjeux éthiques de la recherche en 
sciences sociales. 

De nombreux experts remettent en question sa pertinence et sa sévérité.


Deux ans de suspension, dont une année avec sursis. La sanction du CNRS prise 
le 12 novembre 2024 à l'encontre de Caroline Guibet Lafaye s'avère la plus 
sévère infligée au cours des six dernières années par l'organisme public de 
recherche scientifique.

La peine de la sociologue appartenant au LISST (Laboratoire Interdisciplinaire 
Solidarités, Sociétés, Territoires) à Toulouse (Haute-Garonne) ne concerne 
pas des faits de falsification de résultats de recherche, ni des questions 
de harcèlement sexuel ou de détournement d'argent. 

La directrice de recherche est condamnée pour des "manquements graves à l'
intégrité scientifique". 

"J'ai la chance d'être la personne qui a été la plus lourdement sanctionnée", 
ironise cyniquement la chercheuse de 52 ans, profondément touchée et ébranlée 
par cette exclusion.

Une sanction sans précédent
===================================

La décision sidère au sein du milieu de la recherche : "C’est incroyable, 
réagit François Sicot, professeur de sociologie au LISST. 

Nous n'avons jamais entendu parler d'une sanction de cette ampleur. 

Cela nous a beaucoup surpris et c'est particulièrement violent."

"Une véritable mort sociale" et une déchéance professionnelle pour l'universitaire, 
reconnue pour ses recherches sur les questions de radicalisation et de terrorisme, 
notamment celle des combattants de l'ETA (Euskadi ta Askatasuna) au Pays basque.


"Le travail de Caroline Guibet Lafaye, que je connais bien et que je cite 
régulièrement dans mes propres travaux, est basé sur des données fiables et 
une méthodologie rigoureuse, témoigne le sociologue Jérôme Ferret dans une 
lettre de soutien. 

Il contribue en outre significativement à l'avancement de notre domaine de 
recherche sur des objets sensibles à forte résonance dans la vie des sociétés."

Cette mise au pilori est d'autant plus incompréhensible pour ces scientifiques 
qu'une "chape de plomb" pèse sur cette affaire. 

Au sein du Laboratoire Interdisciplinaire Solidarités, Sociétés, Territoires (LISST), 
il est ainsi interdit de parler ou d'évoquer cette suspension. 

Caroline Guibet Lafaye a interdiction d'échanger avec ses collègues et ne 
peut plus accéder à sa boîte mail professionnelle. 

Elle n'est plus autorisée à travailler. 

Contacté, le directeur du LISST, Michael Pouzenc, regrette ainsi de 
"ne pouvoir que nous orienter vers le service de communication" du CNRS.

Le Kurdistan : une thématique jugée hautement sensible
============================================================

De son côté, le Centre national de la recherche scientifique ne répond pas. 

Le sujet est considéré comme hautement "sensible" en raison de la thématique 
des travaux de Caroline Guibet Lafaye : l'engagement clandestin au sein du PKK, 
le Parti des travailleurs du Kurdistan. 

Si potentiellement dangereux que la condamnation elle-même a totalement été 
anonymisée avant sa publication au Bulletin officiel du CNRS

Ces dernières années, la sociologue s'est consacrée à l'étude de
l'organisation séparatiste armée du PKK (Partiya Karkerên Kurdistan). En 2017
au Kurdistan, elle organise des entretiens avec des membres et d'ex-militants
du PKK racontant les raisons de leur engagement dans la lutte armée en Syrie
et en Irak. Pour ce faire, Caroline Guibet Lafaye embauche deux assistants de
nationalité turque : Barış T., à l'époque doctorant à l'EHESS, et Neslihan
Y., étudiante. Tous deux mènent 64 entretiens, préparés et calés avec Caroline
Guibet Lafaye, en kurde et en turc, pour ensuite les traduire. La récolte,
fructueuse, aboutit à la publication par la chercheuse toulousaine de 5 articles
dans différentes revues scientifiques.  

Des accusations d’invisibilisation et une anonymisation défaillante
=========================================================================

Mais en juillet 2022, Barış T. effectue plusieurs signalements auprès de
ces journaux et se revendique être le co-auteur des papiers publiés sous la
signature de Caroline Guibet Lafaye. Le doctorant alerte également le CNRS
et l'Université de Toulouse. La sociologue est accusée d'avoir invisibilisé
le travail du jeune chercheur dans ses publications, en ne le mentionnant pas
notamment comme co-auteur.

Trois mois plus tard, en octobre 2022, Neslihan Y., appuyée par Barış T.,
porte de nouvelles allégations à l'encontre de la chercheuse. Elle l'accuse de
ne pas l'avoir rémunérée et d'avoir mis en danger les personnes interviewées
en divulguant leurs pseudonymes de guerre dans ses articles. Deux anciens membres
du PKK lui auraient téléphoné pour lui expliquer avoir dû quitter l’Irak
où ils se trouvaient.

Une enquête interne au sein du CNRS, menée par trois experts, est alors ouverte
après la rédaction d'un rapport du Comité d'éthique de l'Association française
de science politique (AFSP). Ils concluent à la suspension, en novembre 2024,
de Caroline Guibet Lafaye, par un vote à l'unanimité des 16 membres de la
commission administrative paritaire (CAP) après 9 heures de débat. Le CNRS
conclut à l'existence d'un "important défaut d'anonymisation dans plusieurs
publications de Mme Caroline Guibet Lafaye" et à "un mauvais comportement de
Mme Caroline Guibet Lafaye qui n'a pas reconnu, dans ses différentes publications,
le travail accompli par chacun des contributeurs, et en particulier celui de
Neslihan Y. qui a réalisé les entretiens." 


"Aucune preuve apportée"
==============================

Ces assertions, Caroline Guibet Lafaye les dénonce et réfute l'hypothèse de
"doctorants lésés qui débinent une "mandarine"" pour se venger, comme
certains observateurs l'évoquent. "Barış T. et Neslihan Y. n'apportent
aucune preuve. L'enquête de la mission de l'intégrité scientifique du
CNRS a été menée exclusivement à charge. Jamais on n'a tenu compte de mes
arguments. Après la publication de mes travaux, le PKK m'a même envoyé un
message pour me remercier d’avoir travaillé sur cette thématique du processus
d’engagement dans la guérilla.", dénonce la sociologue.

Une position que vont défendre ses avocats devant le tribunal administratif de
Toulouse, le 12 février 2025, dans le cadre d'une procédure en référé –
en urgence - afin de suspendre cette condamnation.

"Caroline Guibet Lafaye n'a pas commis de fautes, ou dans tous les cas de
faute majeure, qui justifie une telle sanction qui nous semble totalement
disproportionnée en comparaison des procédures disciplinaires du CNRS au
cours des dernières années", estiment Me Laurent Pasquet-Marinacce et Youri
Krassoulia.  

Co-écrire pour co-signer
============================

L'absence de "charte" au CNRS concernant la signature d'articles scientifiques
est notamment pointée du doigt. "En Sciences Humaines et Sociales (SHS), il
n'existe pas de "politique" et de normes mais seulement des pratiques très
encadrées, témoigne le sociologue Jérôme Ferret. Pour le dire de la manière
la plus simple : pour co-signer, il faut co-écrire." Dans l'un des articles
incriminés, paru dans la revue Violence, l'éditeur n'a ainsi pas voulu ajouter le
nom de Barış T., se contentant de le citer dans une note comme ayant participé
à la recherche. "Ce ne sont que des usages, soulignent les avocats de Caroline
Guibet Lafaye. Ce manque ouvre la voie à de possibles décisions arbitraires."

Quant au sujet des problèmes d'anonymisation, Caroline Guibet Lafaye et ses
conseils remettent en cause la réalité même de la mise en danger des personnes
interviewées.

Les deux combattants du PKK ayant contacté Neslihan Y. n'ont ainsi pas été
auditionnés par la commission administrative paritaire du CNRS, dans une volonté
de prudence et de ne pas divulguer leur identité, selon des membres de la CAP.


Des informations déjà connues
==================================

Farhad Khosrokhavar, Michel Wieviorka, Pierre Blanc : plusieurs spécialistes
viennent mettre à mal ces accusations à travers leurs témoignages apportés
par la défense devant le tribunal administratif.

Alain Chouet, autre haut cadre de la DGSE jusqu'en 2007, affirme que les noms des
Kurdes mentionnés dans les articles en question "étaient connus, parfois depuis
longtemps, par les services spécialisés et, pour la plupart d'entre eux, par la
presse [et] accessibles en sources ouvertes". Il ajoute : "Je distingue mal en
quoi la révélation dans le cadre de la recherche universitaire de l'identité
réelle et/ou fictive de leurs objectifs, qu'ils connaissent parfaitement,
peut constituer une aggravation du risque que courent ces derniers."

Alain Juillet, également ancien directeur du renseignement de la DGSE, abonde
dans le même sens : "Il est intéressant de voir que nombre de ceux qui sont
sortis de la clandestinité ont gardé leur nom de guerre pour communiquer sur
internet comme le docteur Suleyman ou dans la presse écrite comme le docteur
Ali. D'autres ont gardé leurs pseudos comme Sakine ou Agit mais cela ne leur
permet pas de se différencier d'autres sauf lorsqu'ils décident de se montrer
dans les médias ou les réseaux sociaux, ce qui les rend identifiables en
sources ouvertes par tous les chercheurs et par les services."

Le politologue, membre de l'EHESS, Olivier Roy, défend également Caroline Guibet
Lafaye : "Dans les réponses des ex-membres du PKK (...) il n'y a aucun élément
qui pourrait être utilisé à charge par leurs adversaires contre eux ou elles et
aucune information qui pourrait mettre en danger d'autres personnes. Les données
présentées par son accusateur, Monsieur (Barış T.) dans sa propre thèse,
concernant les mêmes personnes que celles que mentionne Madame Guibet-Lafaye,
sont bien plus précises que celles présentées par elle."

Me Laurent Pasquet-Marinacce et Youri Krassoulia s'étonnent par ailleurs :
pourquoi si ces données s’avéraient si sensibles Barış T. cosigne en
2022 un article avec Caroline Guibet Lafaye, publié dans Critical Studies on
Terrorism, les reprenant "sans s'être inquiété de cette prétendue mise en
danger" ? Sollicité par mail, Barış T. n’a pas répondu à nos messages.


Le travail de terrain en sociologie de plus en plus contraint
====================================================================

En dehors du cas personnel de Caroline Guibet Lafaye, pour certains, cette
affaire révèle les difficultés désormais rencontrées par la recherche
en matière de sciences sociales. "Nous sommes désormais confrontés à
la nécessité de publier toujours plus sous peine, individuellement, de ne
pas avancer dans nos carrières, ce que l’on nomme "publish or perish",
et de mauvaise évaluation des équipes et des laboratoires, avec de possibles
conséquences sur nos budgets, constate François Sicot. Nous sommes désormais
très contrôlés sur la manière dont nous anonymisons nos données, au risque
d'être sanctionnés par des personnes qui ne connaissent pas forcément les
contraintes spécifiques à nos méthodes d'investigations et vont nous dire :
"Ça, ce n'est pas la règle.""

Cette formalisation administrative des pratiques de sociologie aboutit à des
situations absurdes. Comme pour cette doctorante dont le travail porte sur les
jeux d'argent et de hasard chez les mineurs. Le Délégué à la Protection
des Données (DPO) du CNRS exige qu'elle obtienne le consentement écrit de
parents pour interroger les enfants. Cependant, comme il s'agit d'une pratique
partiellement illégale et cachée des parents, cette exigence rend la recherche
pratiquement impossible. Comment faire avancer dans ces conditions la recherche ?

La problématique devrait alimenter les débats du congrès de l'Association
française de sociologie dont la 11e édition se tiendra en juillet 2025 justement
à Toulouse (Haute-Garonne).

En dehors de la procédure devant la justice administrative, Caroline Guibet
Lafaye attaque ses accusateurs en ayant déposé plainte pour diffamation.



