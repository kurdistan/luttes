.. index::
   pair: Travailleur alpin ; 2022-12-24

.. _travailleur_alpin_2022_12_24:

=======================================================================================
Grenoble. Rassemblement en hommage aux victimes kurdes d’un acte criminel à Paris
=======================================================================================

- https://travailleur-alpin.fr/2022/12/25/grenoble-rassemblement-en-hommage-aux-victimes-kurdes-dun-acte-criminel-a-paris

:download:`article_travailleur_alpin.pdf`


Vive émotion rue Félix Poulat à Grenoble samedi 24 décembre à Grenoble !
============================================================================


.. figure:: images/communaute_kurde.png
   :align: center


La veille, vendredi 23 décembre, au siège du CDKF, lieu militant de la
communauté kurde et dans deux boutiques tenues par des Kurdes, trois
personnes ont été tuées, plusieurs autres blessées.

Les trois :ref:`victimes décédées sont une femme et deux hommes <assassinats_2022_12_23>`.

Selon :ref:`Agit Polat <agit_polat>`, le porte-parole du :ref:`CDKF <cdk_fr>`, la victime est :ref:`Emine Kara  <emine_kara>`,
responsable du mouvement de femmes kurdes en France.

:ref:`Berivan Firat <berivan_firat>`, une porte-parole du :ref:`CDKF <cdk_fr>`, présente à Grenoble il y a
quelques jours (:ref:`Le 27 novembre 2022 àSaint Martin d'Hères <berivan_firat_2022_11_27>`),
nous a indiqué qu’Emine Kara avait pris part au combat contre l’organisation
Etat islamique (EI), les armes à la main.

Elle avait depuis demandé l’asile politique en France et s’était vu refuser
le statut de réfugiée par l’Ofpra, décision dont elle avait fait appel.

Les deux hommes sont un :ref:`chanteur kurde (Mîran Perwer (Sirin Aydin son vrai nom) <miran_perwer>`,
réfugié politique en France, et :ref:`un vieil habitué (Abdulrahman Kizil, NDLR) <abdullahman_kizil>` du centre culturel Ahmet-Kaya


Une minute de silence a été observée, rue Félix Poulat, vers 14h30 en
hommage aux victimes, tandis que plusieurs centaines de personnes étaient
venues à l’appel de l’association AIAK, entre 14h et 15h.


.. figure:: images/rassemblement.png
   :align: center



L’émotion était grande. La communauté kurde a répondu en quelques heures
à l’appel au rassemblement. Les Kurdes ont peur. Ils sont attaqués par
les bandes fascistes des « loups gris », menacés depuis la Turquie,
traqués parfois par la police française.

Les militants, ce jour veille de Noël, n’étaient pas à la fête, tandis
que des milliers de personnes finissaient leurs emplettes au centre ville.
Ils étaient graves commentant cet attentat qui met en évidence beaucoup
d’interrogations, avec cet assassin qui a pu, huit jours après sa sortie
de prison, organiser son action de mort.

Dire que cet individu n’a pas été prévenu du report de dernière
minute d’une réunion importante de hauts responsables kurdes sur le lieu
de la fusillade

Quel aurait été le bilan de l’attaque ?
=========================================

Maryvonne co présidente :ref:`d’AIAK <aiak>` a donné lecture d’un communique de la CNSK
(Coordination nationale de solidarité Kurdistan)

La CNSK condamne cette odieuse agression qui a eu lieu à 15 jours du 10e
l’anniversaire de l’assassinat à Paris, dans la nuit du 9 au 10 janvier 2013,
de trois militantes kurdes.

Elle apporte son soutien aux familles des victimes et à l’ensemble de la
communauté kurde de France. Après les bombardements de l’armée turque
sur des objectifs civils au Kurdistan de Syrie, l’aggravation de la
répression en Turquie et en Iran, il n’est pas acceptable, ni tolérable que dans
notre pays la sécurité de femmes et hommes réfugié.e.s politiques du fait
de la répression dont ils sont victimes dans leur pays ne soit pas assurée.

**Ce drame est à rapprocher des multiples agressions dont se sont rendu
responsables, sur le sol national, les fascistes turcs des Loups gris
sans que les réponses judiciaires et politiques appropriée n’aient été apportée**.

Toute la lumière doit être faite, par l’instruction en cours, pour déterminer
les conditions qui ont permis à l’auteur des faits d’agir librement alors
qu’il est sous le coup de poursuites judiciaires.

Puis une représentante de la LDH (Ligue des Droits de l’Homme ) est intervenue :
Même si la personnalité du suspect n’est pas encore établie avec certitude,
la LDH constate l’accroissement sans précédent des violences politiques
xénophobes sur l’ensemble du territoire.

Des personnes sont visées en raison de leur origine, de leur confession
ou de leur appartenance politique.

Il ne fait pas de doute que les idéologies racistes, antisémites, xénophobes,
anti-immigrés et réactionnaires, largement portées par l’extrême droite,
sont désormais diffusées sans entraves tant par les médias que par nombre
de personnalités politiques, y compris au sein de l’hémicycle de l’Assemblée nationale


Françoise Breffort, pour le PCF est intervenue :

.. figure:: images/francoise_breffort.png
   :align: center


... Depuis plusieurs années, les autorités françaises qui se succèdent
collaborent avec Ankara pour persécuter la communauté kurde de France.
En voici les résultats !

Les Kurdes, qui agissent depuis toujours de manière pacifique dans notre
pays, doivent être protégés.

Ce sont des hommes et des femmes qui luttent pour la liberté, l’égalité
et le progrès humain. Ils sont en première ligne dans la lutte contre
l’obscurantisme djihadiste.

Ils sont nos amis et nous sommes indéfectiblement à leurs côtés.

Michel Szempruch a apporté le soutien du NPA et une militante celui du
syndicat Solidaires.

Le rassemblement s’est poursuivi avec celui des militantes et militants
iraniens et de leurs soutiens contre les tueries dont sont victimes les
Iraniens en lutte contre leurs gouvernants.


