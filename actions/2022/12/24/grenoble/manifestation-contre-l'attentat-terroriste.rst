
.. _manif_grenoble_2022_12_23:

===========================================================================================================================================
Samedi 24 décembre 2022 **Manifestation contre l'attentat terroriste contre le siège du CDK-F à Paris**
===========================================================================================================================================



Communiqué AIAK
=================

.. toctree::
   :maxdepth: 3

   aiak/aiak


Informations AKIA
===================

Bonjour

Vous trouverez ci- dessous une vidéo de place gre'net, un article de france
bleu isère et un article paru sur le site du travailleur alpin sur le
rassemblement de samedi 24 décembre suite à l'attaque meurtrière du 23
décembre à Paris faisant 3 morts et 3 blessés .

- Place gre'net `Rassemblement en soutien au peuple Kurde à Grenoble - YouTube <https://www.youtube.com/watch?v=9TWy6rd4h2A>`_
- radio france isère Grenoble : `environ 80 personnes réunies en soutien aux Kurdes (francebleu.fr) <https://www.francebleu.fr/infos/societe/grenoble-environ-80-personnes-reunies-en-soutien-aux-kurdes-2718000>`_

Sur Youtube
=============

.. figure:: images/sur_youtube.jpeg
   :align: center

   https://invidious.weblibre.org/watch?v=9TWy6rd4h2A


Quelques photos
=================


.. figure:: images/halte_a_la_sale_guerre_contre_les_kurdes_2.jpeg
   :align: center


.. figure:: images/kurdes_devant_eglise.jpeg
   :align: center

.. figure:: images/kurdes_devant_eglise_2.jpeg
   :align: center

.. figure:: images/drapeaux_kurdes.jpeg
   :align: center


.. figure:: images/paix_au_rojava.png
   :align: center

.. figure:: images/stop_erdogan.jpeg
   :align: center


.. figure:: images/interview.jpeg
   :align: center


Article du travailleur alpin
==============================

.. toctree::
   :maxdepth: 3

   travailleur_alpin/travailleur_alpin
