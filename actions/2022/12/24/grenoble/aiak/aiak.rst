

=====================
Communiqué de AIAK
=====================

- https://www.orion-hub.fr/w/iwCmM9stmfVBxAcHv8uhdC


Appel à la manifestation
===========================


Attaque contre le CDKF

Bonjour,

AIAK apporte ses plus sincères condoléances aux victimes et à leurs proches
et demande à ce que justice soit faite face à ce terrible attentat.

Nous appelons à un rassemblement ce samedi 24 décembre 2022 à 14h00
rue Felix Poulat 3800 Grenoble

Ce rassemblement marquera la volonté de celles et ceux qui y participeront :

- de rendre hommage aux victimes de l’attentat de Paris le 23 décembre 2022
- d'apporter notre solidarité avec la communauté Kurde
- de demander aux autorités françaises de renforcer la protection de la
  communauté kurde et de combattre les agissements violents, notamment
  encouragées par les autorités gouvernementales turques

Pour aiak Maryvonne Matheoud et Ali Arslan co-présidents


Communiqué au nom du CNSK
==============================

Voir le communiqué au nom du CNSK ici: :ref:`aiak_2022_12_23`
