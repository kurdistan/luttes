
.. _ref_assassinat_kurdes_2022_12_23:

===========================================================================================================================================
Vendredi 23 décembre 2022 **Attaque terroriste contre le siège du CDK-F à Paris**  #JesuisKurde #Paris10 #ParisAttacks #TwitterKurds
===========================================================================================================================================

- :ref:`assassinat_kurdes_2022_12_23`

#FreeKurdistan #JesuisKurde #Paris10 #ParisAttacks #MastodonKurds
#JesuisKurde #Paris10 #ParisAttacks #TwitterKurds #AttentatTerroriste #Paris #Defendkurdistan #le2412 #ParisMassacreOfKurds  #Justice4Kurds
#noel #Christmas #paris10 #JeSuisKurde #Justice4Kurds #MastodonKurds #TwitterKurds #JinJiyanAzadi #ParisAttacks



Sur Youtube
=============

- https://invidious.baczek.me/watch?v=1GOXcE-Isx4


.. _retranscription_cnt_2022_12_23:

Conférence de Presse, la retranscription du 23 décembre 2022
===============================================================

- :ref:`cnt_si_2022_12_29`
- https://international.cnt-f.org/Conference-de-presse-du-Conseil-Democratique-Kurde-en-France-le-23-dec-2022-Paris.html

.. figure:: images/agit_polat.png
   :align: center
   :width: 400

   :ref:`agit_polat`

Mesdames et messieurs les journalistes, bonsoir, merci de votre très forte présence.

Comme vous le savez tous aujourd’hui encore une fois en plein cœur de Paris,
dans le dixième arrondissement, la communauté kurde a été frappée par un
attentat **terroriste**, de caractère **terroriste**, en ce qui concerne notre
définition de cette attaque.

Encore une fois malheureusement la communauté kurde a été meurtrie, en France,
à Paris, et encore une fois dans ce même dixième arrondissement.

Nous étions depuis quelques temps en train de préparer le dixième anniversaire
du :ref:`triple assassinat des militantes kurdes <assassinats_2013_01_09>` qui ont été exécutées à Paris
le 9 janvier 2013, et de ce fait encore une fois nous avons encore été
frappés ici.

La situation politique en Turquie, les évolutions politiques concernant
le mouvement kurde, concernant la communauté kurde, le peuple kurde,
nous laisse **très clairement** penser que ce sont des assassinats encore
une fois politiques.

Par **le biais de la presse** nous avons appris aujourd’hui que le caractère
terroriste dans le cadre de la procédure n’avait à ce stade pas été retenu.

**Nous sommes actuellement indignés ! Indignés face à cette situation.**

**Il est inadmissible qu’aujourd’hui, dans le cadre de cette affaire le
caractère terroriste ne soit pas retenu**, et qu’on essaye de nous faire
croire qu’il s’agit d’un simple militant d’extrême droite, un récidiviste,
qui vient tout juste de sortir de prison le 12 décembre, et vient commettre
cet horrible attentat dans nos locaux du Conseil Démocratique Kurde en
France, dans le siège de cette fédération qui rassemble des dizaines
d’associations kurdes de toute la France.

C’est pourquoi nous faisons encore une fois appel aux autorités françaises :

- **Arrêtez votre coopération avec les services de renseignements turcs !**
- **Arrêtez votre complaisance !**
- Face à l’impunité, arrêtez votre complaisance avec les autorités turques
  quand il s’agit de la sécurité des militants kurdes !


`L’une des victimes (Emine Kara, NDLR) <emine_kara>` aujourd’hui assassinée était l’une des responsables
du mouvement des femmes kurdes en France ! C’est pourquoi il y a un
caractère **politique** ! C’est pourquoi il y a un caractère **terroriste** !
Que personne n’essaye de nous faire croire qu’il s’agit d’un simple attentat
orchestré par l’extrême droite !

**Je suis obligé de faire ce constat**, jusqu’ici depuis plusieurs décennies,
la communauté Kurde n’a jamais été prise pour cible par l’extrême droite.
Qu’est ce qui a changé aujourd’hui particulièrement à la veille du dixième
anniversaire de ces assassinats pour que l’extrême droite ait commencé
à s’en prendre à la communauté kurde au cœur de notre organisation, de
notre institution ?

Selon notre première constatation il se peut éventuellement que cette
personne ait été retournée en prison par des membres djihadistes, par
des membres des services de renseignements turcs en prison.
Il n’est pas normal, il est incompréhensible qu’une personne là s’en
prenne de cette façon là à seulement dix jours après sa libération à
la communauté kurde en plein cœur de Paris.
Et encore une fois la veille du dixième anniversaire du triple assassinat
des militantes kurdes.

Nous attendons des autorités françaises que la lumière soit faite, et toute
la lumière soit faite sur cette affaire !
Nous avons un souvenir **h-o-r-r-i-b-l-e**, qui dure aujourd’hui depuis
maintenant dix ans concernant le 9 janvier 2013 !
Jusqu’ici les autorités françaises **n’ont pas reçu une seule fois** les
familles des victimes du triple assassinat de 2013 !
Jusqu’ici les autorités françaises **ne nous ont pas reçus**, nous, en tant
que :ref:`CDK-F <cdk_fr>`, en tant qu’organisation kurde concernant ces assassinats !

Jusqu’ici les services de renseignements français **n’ont pas assuré notre
sécurité** !
Il y a tout juste vingt jours de ça, lors d’un échange avec les services
de renseignements français, **j’ai moi-même personnellement fait part de
mes craintes concernant notre sécurité**.
Il y a tout juste vingt jours de ça, **j’ai souligné la nécessité** d’augmenter
la sécurité autour et au sein de notre association !

Comment se fait-il encore une fois que malgré nos alertes, que malgré
ces cris de détresse, que malgré cette tentative au gouvernement de
faire entendre notre voix, comment se fait-il encore une fois qu’on
soit meurtris ici ! Qu’on soit assassinés de nouveau sous la responsabilité
des autorités françaises ?!

L’une des personnes assassinées aujourd’hui (:ref:`Emine Kara (NDLR) <emine_kara>`) avait entamé une procédure
de demande d’asile auprès de l’État français.
**Pourquoi cette personne s’est vue refusée [l’asile] ?!**
Malgré que les autorités françaises savaient que c’était une militante
kurde.
**Pourquoi** est-ce que cette femme aujourd’hui n’a pas été protégée ?!
**Pourquoi** est-ce que ces deux autres membres de notre association militant
activement dans notre association n’ont pas été protégés ?!

Pourquoi ces trois autres personnes sont aujourd’hui dans cette situation ?!
Il y a des questions et il y a des points d’interrogations qui demeurent
toujours ! Les autorités françaises doivent nous recevoir !
**Les autorités françaises doivent arrêter ce jeu cynique !**

Le 9 janvier 2013 les :ref:`assassinats ont survenu <assassinats_2013_01_09>`, le 20 janvier le ministre
de l’intérieur de l’époque, Manuel Valls, est allé rencontrer l’ambassadeur
turc de l’époque. **Nous avons les échanges**.
**Nous avons reçu la note d’échanges qui a eu lieu**. Manuel Valls disait 11
jours après les assassinats des militantes kurdes en France, qu’ils
**allaient continuer leur coopération pour lutter contre le P.K.K** !

- Qui est terroriste alors dans cet État ?!
- Qui est-ce qui a agi en tant que terroriste dans ce pays ?!
- Est-ce que c’est le P.K.K.?
- Est ce que c’est le mouvement Kurde ?
- Est-ce que c’est la communauté kurde qui a dégradé, qui a tué ?
- Est-ce-que c’est nous ?

Ou bien est-ce que ce sont les **services de renseignements français** avec
lesquels les autorités françaises coopèrent et ne lèvent pas le secret
défense pour faire toute la lumière dans cette affaire ?!

Qu’est-ce que la France a à se reprocher quand il s’agit de la levée de
ce secret défense ?
Pourquoi est-ce que la France ne lève pas aujourd’hui le secret défense
depuis dix ans maintenant ?

Si le P.K.K. n’avait pas été sur la liste des organisations terroristes,
ni le 9 janvier, ni aujourd’hui encore une fois, ces assassinats ne
seraient survenus.
Nous ne sommes pas bêtes, nous ne sommes pas stupides, nous ne sommes
pas dupes !
**Nous savons de quoi il s’agit**.

- **Nous savons très bien qui a orchestré ces assassinats !**
- Nous savons que Erdögan et l’État turc sont derrière ces assassinats !
- Que les autorités françaises n’attendent pas de nous de croire à leur version !

Ils vont faire toute la lumière et ils sont obligés de faire toute la
lumière concernant cette affaire !

Et comme concernant les affaires du 9 janvier 2013.

C’est pourquoi **nous sommes en colère** !

- **Nous sommes en colère** parce que nous avons alerté à de multiples reprises
  et la dernière fois c’était tout juste il y a vingt jours de ça !
- **Nous sommes en colère** parce que nous n’avons pas été entendus !
- **Nous sommes en colère** car nous nous faisons constamment arrêter !
  Nous nous faisons constamment réprimer par les autorités françaises !

Qu’avons-nous fait aux autorités françaises ? **N’est ce pas pas nous qui avons
combattu contre Daesh pour sécuriser aussi à la fois la sécurité et la
stabilité de ce pays, de la France ?**

Rappelez-vous de 2014, de 2015, tout le monde applaudissait le peuple kurde.
Tout le monde applaudissait les combattants kurdes.
Tout le monde parlait du courage des femmes kurdes qui ont combattu
contre Daesh.
Les autorités françaises le savent très bien: qui a lutté là-bas au Rojava,
en Syrie, au Kurdistan Irakien contre Daesh !

Les autorités françaises et les autorités militaires le savent très bien
qui sont ces guérilleros, qui sont ces combattants qui luttent contre Daesh !

Donc on est arrivé à un stade où il faut arrêter de faire le dupe, où il
faut arrêter de jouer aux trois singes.
Nous n’acceptons plus cette politique d’État de la France ! Nous n’acceptons
plus cette politique qui divise le peuple kurde !

Je lance solennellement un appel au Président de la République : il y a
besoin d’une autorité politique !
Il y a besoin d’une volonté politique nette, claire, précise, de la part
de la France ! Ne pas laisser la question kurde en tant qu’otage de la
diplomatie française.
Ne sacrifiez pas cette cause ! Il s’agit du destin de tout un peuple !
De 50 millions de Kurdes ! De 50 millions de Kurdes… on parle du destin
de 50 millions de Kurdes..

Vous ne pouvez pas laisser ce dossier, ce sujet, à la merci de la
diplomatie française !
Vous ne pouvez pas marchander sur le dos du peuple kurde de cette façon là !
Arrêtez-vous ! Stop ! On en a marre de cette politique !
Il y a besoin que la France développe, élabore une politique propre,
une politique kurde, une politique inclusive !
Il faut que la France fasse appel pour la paix ! Il faut que la France
contribue pour la résolution de la question kurde. C’est ainsi que je
lance mon appel au président de la République : il y a besoin d’une
volonté politique !
Il y a besoin d’une volonté politique !
Il y a besoin d’une volonté politique !

Mesdames et messieurs c’est ce que j’ai à dire, la communauté kurde est
endeuillée, et **jusqu’à ce que crime ne sera pas élucidé encore une fois,
notre deuil se poursuivra**.

Jusqu’à ce que la lumière ne sera pas faite concernant cette affaire,
nous continuerons notre quête de justice, comme nous la continuerons
pour le 9 janvier 2013. Demain à midi il y aura un grand rassemblement
sur la place de la République, nous allons manifester notre colère
encore une fois !
Des dizaines de milliers de Kurdes vont défiler dans les rues de Paris
encore une fois ! Notre colère va être dans les rues de Paris !

Il faut que les autorités françaises nous écoutent !
Il faut que les autorités françaises ne retombent pas dans la même erreur
qu’elle est tombée le 9 janvier 2013.

- Il s’agit **très nettement, très clairement d’un attentat terroriste** ;
- il s’agit **très nettement et très clairement d’un attentat politique** ;
- il s’agit **très nettement et très clairement d’un féminicide à la fois** !

C’est pourquoi nous allons continuer notre combat, nous n’avons pas peur !

Que l’État turc, que Erdögan, que les commanditaires le sachent très bien :
nous n’avons pas peur !

Nous allons continuer notre combat, nous allons continuer pour la liberté,
pour la démocratie, pour les droits humains, nous allons continuer ce
combat pour la liberté universelle !

C’est pourquoi nous n’avons pas peur et nous allons poursuivre jusqu’au
dernier qui restera !

Je vais laisser la parole à notre avocat, maître Amdic, qui va aussi faire
un point juridique sur la situation.


Maître David Amdic (avocat du CDK-F) 
========================================

- https://youtu.be/1GOXcE-Isx4?t=681

.. figure:: images/david_amdic.png
   :align: center

Oui bonjour, je suis l’avocat du CDK-F, David Amdic, avocat au barreau
de Paris.

Beaucoup d’émotion aujourd’hui, beaucoup de colère aussi on a pu le voir…
Trois femmes ont été assassinées il y a moins de dix ans.
Aujourd’hui trois militants de la liberté ont été assassinés, trois blessés,
nous ne connaissons pas encore le sort qui leur est réservé.

Désolé je parle... je suis un peu ému car je fais partie des premiers,
avec Agit Polat à m’être rendu sur la scène de crime où ce massacre a
eu lieu.
On parle d’assassinat… je rejoins les propos d’Agit Polat, on parle
d’assassinat, et non pas d’attentat terroriste.
C’est assez étrange. Vous savez je parle en ma qualité d’avocat, je
défends des personnes, des militants de la liberté, kurdes, qui, pour
avoir manifesté, pour avoir parfois dégradé des biens, sont poursuivis
en France avec la qualification terroriste.
On parle de jets de pierres, de jets d’autres choses, et ces personnes
là sont poursuivies pour terroristes.

Aujourd’hui on nous parle d’assassinats, on n’a pas vraiment mis les mots,
et on refuse de retenir la qualification terroriste.

**Ça, ça suscite de la colère. Ça suscite beaucoup d’incompréhension**.

Il y a dix ans, moins de dix ans, trois femmes ont été assassinées.
Depuis dix ans il y a un déni de justice. C’est ça la réalité que vit la
communauté franco-kurde de France. Un déni de justice permanent, en Turquie,
et maintenant elle commence à le ressentir en France.
On parle de personnes, monsieur Agit Polat disait tout à l’heure que, la
plupart ou certains en tous cas étaient réfugiés politiques. Ces personnes
ont quitté des pays en guerre, pour ceux qui viennent du Nord Syrie,
du Rojava – le Kurdistan Syrien, d’autres la Turquie pour fuir des persécutions
politiques (je les défends également dans le cadre de mon travail).

**Et ces personnes-là viennent en France chercher r-e-f-u-g-e**.

Elles fuient la mort, elles fuient les persécutions de ces États pour
trouver r-e-f-u-g-e. Et finalement ce qu’elles ont trouvé ces personnes,
ce n’est pas refuge... c’est la mort.
Voilà le message qu’on envoie, la communauté kurde aujourd’hui a peur.
Elle était déjà traumatisée par ce triple assassinat qui est intervenu
il y a moins de dix ans à quelques rues d’ici. À quelques rues d’ici, oui.
Et aujourd’hui… Aujourd’hui, énième atrocité commise contre le peuple
kurde, non pas en Iran, non pas en Irak, non pas en Syrie, non pas en
Turquie… mais en France.

La communauté franco-kurde est aujourd’hui en colère, et elle a peur.
Elle a besoin de réponses, elle a besoin de justice et surtout, … elle
a besoin de considération.
Parce qu’en effet quand on refuse d’accueillir les familles des trois
femmes assassinées ; quand on refuse l’accès au dossier également,
une grande partie du dossier, au nom du secret défense… ;
quand depuis ce matin, onze heure, les coprésidents du CDK-F, les représentants
du CDK-F, moi-même avocat du CDK-F, nous essayons en v-a-i-n de
joindre les services compétents pour avoir de l’information, et qu’on
ne nous répond pas ! ...
que l’on doit attendre les dépêches des journalistes pour avoir un semblant
de réponse… eh bien oui à ce moment-là on peut être en colère.

On peut être en colère et on peut se sentir… au moins déconsidérés, si
ce n’est sous évalué, je ne sais pas est-ce que les franco-kurdes sont
des citoyens de seconde zone en France ? Voilà ce que j’entends tout à
l’heure.

Pourquoi n’y a-t-il pas d’escorte policière devant les centres démocratiques
kurdes de France ?
Trois assassinats ont eu lieu et depuis aucune, a-u-c-u-n-e surveillance
policière.
Les Kurdes sont livrés à eux-mêmes et on les assassinent pour certains,
derrière moi, en toute impunité. Voilà ce qui se dit.

.. _berivan_firat_2022_12_23:

Berivan Firat (porte-parole du CDK-F) ,
=======================================================

- :ref:`berivan_firat`

.. figure:: images/berivan_firat.png
   :align: center

   :ref:`berivan_firat` photo ©Merlin Dauget

Moi je voudrais dire quelques mots brièvement. Parce que je pense que ça
a été longuement dit.
Mais encore une fois ici ce sont les Kurdes, les femmes libres, les femmes
libres, celles qui ont combattu et qui combattent toujours contre
l'obscurantisme qui ont été visées.

Vous les journalistes, vous êtes tous dans la rue quand on lance le slogan
« femme, vie, liberté », sachez qu'une des personnes qui a été tuée est
à l'origine de la formation de ce slogan.

Elle a donné toute sa vie pour que ce slogan « femme, vie, liberté » voit
le jour. Ce slogan qui est aussi une philosophie de vie. De ce fait, on
ne peut pas ne pas qualifier cette attaque comme une attaque terroriste,
tout en sachant que l'heure qui a été choisie pour les attaques n'est
pas anodin.

Il y avait ce matin une réunion des femmes kurdes justement.
A l'heure de l'attaque, il y avait une réunion des femmes kurdes pour
justement préparer les actions du 7, du 9 janvier, pour les assassinats
des trois femmes kurdes assassinées en 2013, et à cause des grèves et
des trains qui étaient en retard, cette réunion a été reportée d'une heure.

Dans notre malheur, nous avons la chance que cette réunion ait été
reportée d'une heure sinon ce n'était pas trois personnes dans le centre
culturel kurde mais c'était plusieurs dizaines de femmes qui auraient
été assassinées !
Des femmes libres, des femmes qui essayent ici en France de faire valoir
cette philosophie de la liberté des femmes.

On a un petit message pour les français : sachez que la Turquie n'est
en aucun cas un allié pour la France, pour les intérêts de la France,
pour la diplomatie française.
Par contre les Kurdes qui sont un peuple laïc, qui défendent une politique
multi-ethnique, paritaire, écologique, sont le seul à pouvoir promouvoir
une paix au Moyen-Orient, et qui dit la paix au Moyen-Orient, dit la
stabilité en Occident.

Et une dernière chose, depuis ce matin 11h les Kurdes n'ont pas accès à
leur centre, on comprend, il y a eu des assassinats à l'intérieur.
Les Kurdes n'ont pas accès à leur rue. Mr le Ministre Darmanin vient
faire une déclaration, les journalistes sont invités à aller le rejoindre,
mais pas les responsables du Conseil Démocratique Kurde !

À qui voulez-vous faire une déclaration ? À la presse ? Vous pouviez le
faire dans votre bureau ou vous pouviez le faire ailleurs.
Vous venez devant le centre démocratique kurde, il est de votre devoir,
en tant que responsable français, en tant que ministre de vous adresser
au responsable du Centre Démocratique Kurde, ce centre qui a été visé
et qui a été à l’origine de cette attaque terroriste !

Donc encore une fois nous rappelons aux autorités françaises, qu’il est
temps que non seulement le secret défense sur le dossier de l'assassinat
de Sakine Cansız, Fidan Doğan, Leyla Söylemez soit levé, mais que toute
la lumière soit faite sur cette attaque terroriste !

Qui a été commise je ne sais pas trop, par une personne qui serait
raciste ou suprémaciste.
C'est quand même bizarre que dans la rue d'Enghien il y a des dizaines
d'ethnicités différentes, il y a des africains, il y a des marocains,
il y a des tunisiens, il y a des libyens, mais comme par hasard il
rentre dans le centre kurde, comme par hasard il rentre dans le
restaurant kurde, et comme par hasard il marche 150 mètres pour venir
s'attaquer à un salon de coiffure kurde. Et, encore une fois, ce sont
les Kurdes qui l'ont attrapé, comme ils avaient attrapé Omer Güney,
l'assassin des trois militantes kurdes !

Donc maintenant, la justice est un droit, est une dette envers le
peuple kurde ! Envers le Conseil Démocratique Kurde !
Et la France et toutes ses instances doivent se mettre au travail sans
attente pour que la justice nous soit rendue et que ceux qui sont derrière
cet assassinat, cette attaque terroriste d'aujourd'hui, mais aussi
ceux qui sont derrière les assassinats de 2013, soient traduits devant
la justice pour pas que n'importe qui, les services secrets étrangers,
viennent faire la chasse sur le territoire français, ce qui est aussi
une honte pour la France.


Questions de la salle 
===========================

Pouvez-vous nous confirmer que les assassinats ont eu lieu à l'intérieur du centre kurde ?
---------------------------------------------------------------------------------------------

Ils sont survenus sur le pallier de notre association, devant la porte
d’entrée, sur le pallier au dessus donc oui dans ce sens c’était la
propriété de notre association, on peut de ce fait clairement dire que
c’était à l’intérieur notre association.

Est-ce que le parquet dit quoi que ce soit ?
---------------------------------------------------

À ce stade, on ne sait pas. On ne sait pas, on n'a pas eu d'interlocuteur
de la part des autorités. Le ministre, comme l'a évoqué ma collègue,
est arrivé sur les lieux mais ne nous a pas reçu, n'a pas reçu les
représentants kurdes qui se trouvaient actuellement sur les lieux.

Est-ce-que vous pouvez dire un mot sur les deux autres victimes ?
----------------------------------------------------------------------

L'une d'entre elles était un artiste, :ref:`un artiste kurde exilé aussi (Mîran Perwer, NDLR) <miran_perwer>`.
Réfugié politique, qui était poursuivi en Turquie pour son art, pour
avoir chanté en kurde, donc de ce fait cette personne résidait sur le
territoire national depuis plusieurs années.
Il était régularisé, reconnu en tant que réfugié, donc les autorités
françaises avaient le devoir encore une fois d'assurer la protection
de cette personne mais ça ne s'est pas passé comme ça devait l'être.

La `troisième personne (Abdulrahman Kizil, NDLR) <abdullahman_kizil>`  était également une personne fréquentant très
régulièrement, même quotidiennement, notre association, un membre actif
de notre association. Une personne plus âgée, un citoyen kurde ordinaire
qui avait l'habitude de fréquenter l'association.


La première personne dont vous parlez, la femme, était réfugiée ?
-------------------------------------------------------------------

`La femme n'était pas réfugiée (Emine Kara, NDLR) <emine_kara>`, elle avait fait une demande qui a été
rejetée par les autorités. Encore une fois on voit à quel point les
autorités devaient accorder ce statut-là.
Bien évidemment, on ne sait pas si, même si le statut lui avait été accordé,
est-ce que les autorités allaient pouvoir aussi assurer sa sécurité ?
On a bien vu que non.

[… intervention d’élus...puis reprise des échanges avec la salle]

Quelques mots par rapport à ce que le ministre Darmanin a dit ?
------------------------------------------------------------------

**On ne sait pas ce qu’il a dit. Voilà. C’est la seule chose qu’on peut
dire parce qu’on ne l’a pas vu.**

**Parce qu’il nous a pas reçu, parce qu’il a pas voulu nous recevoir.
C’est aussi clair que ça.**

**Les représentant de la communauté sont ici**.
Nous savons tous que les autorités nous connaissent très bien. Ils nous suivent très bien.

**Donc ils ont nos numéros**. Ils savent qui nous sommes. Nous les rencontrons
aussi. Ils auraient bien pu nous rencontrer. Ils auraient bien pu nous
appeler. Ils auraient bien pu parler avec nous. Il ne s’est rien passé
de cela. Donc c’est pourquoi on ne sait pas ce que le ministre de
l’intérieur a dit.

Juste pour reprendre parce qu’au départ il y a un petit souci sur les  faits de ce qui s’est passé ce matin dans le Centre, vous pouvez nous redire ?
-------------------------------------------------------------------------------------------------------------------------------------------------------

Ce qui s’est passé aujourd’hui c’est qu’un attentat terroriste est survenu
dix ans après le triple assassinat des trois militantes kurdes.
Encore une fois les autorités françaises n’ont pas assuré la protection
de notre structure le Conseil Démocratique Kurde en France, encore une
fois les autorités n’a pas assuré la protection des militants kurdes qui
agissent légalement sur le territoire national et encore une fois nous
avons été meurtris et frappés.

Nous n’acceptons nullement que le caractère terroriste à ce stade ne soit
pas retenu. Nous savons que c’est un attentat politique, terroriste,
orchestré par les autorités turques.
Nulle doute pour nous que les autorités turques sont derrière encore une
fois ces assassinats qui surviennent sur le territoire national.
Nous avions fait part de nos craintes il y a seulement une vingtaine de
jours de ça aux autorités françaises à l’approche du dixième anniversaire,
en vue des évolutions politiques en Turquie, tant comme au Moyen Orient
en lien avec le peuple kurde et le mouvement kurde.

C’est pourquoi dans ce sens là nous pouvons très facilement, très clairement
dire que les autorités françaises n’ont pas accompli leur devoir, ne nous
ont pas pris au sérieux, n’ont pas écouté nos alertes.
Et donc on attend encore une fois des autorités françaises qu’ils accomplissent
leur tâche. On attend des autorités françaises que toute la lumière soit
faite sur cette affaire ; car nous avons un souvenir qui date de dix ans
maintenant et pour le quel les familles seulement, je ne parle même pas
des représentants d’organisations kurdes, les familles seulement n’ont
pas été reçues par les autorités françaises.

C’est une personne importante qui a été assassinée ce matin de la communauté kurde ?
--------------------------------------------------------------------------------------

L’une des personnes était responsable du mouvement des femmes kurdes,
avait des responsabilités à l’échelle nationale au sein du mouvement des
femmes kurdes. Donc oui, comme ma collègue l’a précisé aussi tout à l’heure,
il était sujet d’une réunion qui a été reportée en raison des grèves et
de certains inconvénients.
Une réunion interne encore une fois, il est important de préciser ce
point là.

Comment se fait-il que un militant d’extrême droite aujourd’hui
tue à seulement une dizaine de jours après sa libération, puisse commettre
un attentat comme celui-ci à Paris ?
Pourquoi est-ce qu’il n’était pas poursuivi ?
Pourquoi, comme maître Amdic l’a précisé, les simples jeunes kurdes qui
lancent des pierres ou bien qui lancent des cocktails molotov aussi,
chose que nous n’acceptons pas bien évidemment, mais pourquoi lorsqu’il
s’agit des kurdes on arrive très facilement à retenir le caractère
politique, mais pourquoi est-ce que dans le cadre d’un assassinat
comme celui-ci on arrive pas à retenir le caractère terroriste ?

Pour vous c’est clairement un acte terroriste ? Vous en appelez à rencontrer le président de la république pour cela ?
--------------------------------------------------------------------------------------------------------------------------

Bien sûr que nous en appelons au président de la république, de nous
recevoir, d’écouter la communauté kurde, les représentants de la communauté
kurde. Il est maintenant important, on arrive à un stade où les évolutions
politiques doivent changer.
On arrive à un stade d’un point de vue géopolitique où les intérêts de
la France aussi sont en jeu. On parle d’une Turquie qui est ennemi de
la France, qui est ennemi de l’Europe, qui est ennemi des intérêts de
la France. On le constate dans la méditerranée orientale, on le constate
au Moyen Orient, les chercheurs, les analystes, les analystes militaires l
e savent très bien, et encore mieux que nous.

Donc on parle d’un ennemi aujourd’hui. La France doit clarifier sa politique
vis à vis du mouvement kurde, plus particulièrement du P.K.K…. plus
particulièrement du P.K.K… et je m’en cache pas de parler de ce sujet,
et je n’ai rien à me reprocher de parler de ce sujet.
Il en va aussi des intérêts de la France. Pendant que la Turquie est en
train de mettre ce sujet du terrorisme en avant, les intérêts de la
France sont en train de reculer dans ce sens là.
Et la Turquie a très bien compris ce jeu. Parce que les autorités
françaises ont peur de rencontrer les représentants kurdes.
Parce que les autorités françaises, les services de renseignements
français que ce soit militaire ou DGSE ou DGSI ont peut de les rencontrer,
et pourquoi ?
Parce que la Turquie insiste sur le caractère terroriste. C’est le seul
argument qui fait aujourd’hui trembler les services de renseignements
français, et qui met un barrage en tous cas en face de cette situation.

C’est pourquoi pour conclure et ne pas me répéter encore une fois, il est
important de trouver une solution politique, pacifique, inclusive,
à la question kurde.
Nous en appelons encore une fois au Président de la République de nous
entendre, d’affirmer une volonté politique. C’est seulement le Président
de la République qui peut faire ça.
De ne pas laisser la question kurde à la merci du Quai d’Orsay, à la
service des diplomates, et à la merci des services de renseignements
qui coopèrent , qui marchandes sur notre dos.

Vous aviez demandé de la protection ?
------------------------------------------

Nous n’avions pas demandé de protection particulièrement ou bien constante,
mais nous avons fait part de nos craintes, nous avons dans ce sens là de
manière générale demandé effectivement que des mesures de sécurité soient
déployées dans ce sens là.

Merci d’être venu.



