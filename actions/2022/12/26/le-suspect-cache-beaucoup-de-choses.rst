
.. _agit_polat_2022_12_26:

====================================================================
2022-12-26 Agit Polat : **Le suspect cache beaucoup de choses**
====================================================================

- :ref:`agit_polat`
- https://yewtu.be/watch?v=Vckqv0_y9_M

.. figure:: images/agit_polat_1.png
   :align: center


.. figure:: images/agit_polat_2.png
   :align: center


Quatre jours après l'attaque qui a tué trois personnes kurdes dans le
centre de Paris, l'enquête avance et la communauté touchée fait porter
sa voix.

Selon Agit Polat, porte-parole du Conseil démocratique kurde en France,
l'attaque devrait être qualifiée d'attentat terroriste car le suspect
visait particulièrement la communauté kurde, voulait tuer beaucoup plus
de personnes et pourrait avoir des complices :

"Il cache beaucoup de choses car il y a beaucoup de confusion dans ses
propos. Le fait que cette personne porte au moins 60 munitions, nous
laisse penser qu'il a voulu faire un carnage, un nettoyage ethnique
dans nos locaux. 

On n'a pas ciblé n'importe qui. Je comprends que la justice se base sur
des faits concrets mais nous avons déjà vécus des assassinats politiques
orchestrés par la Turquie en 2013. 

Au bout de quelques semaines ou mois, on pense que des éléments concrets
vont tomber."

