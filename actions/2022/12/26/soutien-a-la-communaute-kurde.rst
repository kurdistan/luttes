
.. _soutien_2022_12_26:

================================================
2022-12-26 Soutien à la communauté kurde
================================================

- https://cric-grenoble.info/infos-locales/article/soutien-a-la-communaute-kurde-2731


Introduction
=============

Loin des manifs fortement réprimées comme à Paris ou Marseille, c’est un
rassemblement dans le calme réunissant une petite centaine de personnes
qui a eu lieu ce samedi 24 décembre, place Félix Poulat à Grenoble, en
hommage aux 3 militant.es kurdes assassinés à Paris la veille.

En effet l’association culturelle kurde Ahmet Kaya dans le 10e arrondissement
a été la cible d’une attaque armée, à l’heure où une réunion avec une
soixantes de personnes attendues était prévue pour préparer la commémoration
des 3 victimes de 2013.

Entre plusieurs slogans dénonçant la politique tyrannique d’Erdogan,
des chants se font entendre, comme l’hymne du mouvement de libération
du Kurdistan "çerxa soresê" ("des murs des prisons au sommet des montagnes,
levez le drapeau rouge ; la résistance est la vie"), ou encore le chant
pacifiste arabe Holm de Emel (qui était la voix de la Révolution du jasmin
en Tunisie en 2010-2011).

Des prises de paroles de syndicats et de l’AIAK, Association Iséroise
des Amis du Kurdistan, mettent en accusation le gouvernement français
de ne pas reconnaître le caractère terroriste de l’attaque, et rappellent
aussi que la Turquie mène une guerre au sud-kurdistan et au Rojava, en
multipliant notamment ces derniers mois des frappes aériennes, des drones,
des armes chimiques, mais aussi par des arrestations et perquisitions.

Nous retranscrivons ici le discours d’une des militantes de l’AIAK 
=====================================================================

C’est la deuxième fois en 10 ans que Paris devient la scène d’un massacre
politique mené contre les kurdes et les femmes kurdes.

La 9 janvier 2013, trois militantes politiques kurdes ont été assassinées
par les forces de renseignement turques. Malheureusement, l’état français
n’a pas assumé ses responsabilités en enquêtant et en tenant pour
responsables les auteurs de ces meurtres.

Les véritables coupables n’ont jamais été traduits en justice en raison
de la réticence du gouvernement français à rechercher la vérité.
Nous, en tant que congrès national du Kurdistan, ne considérons pas cette
attaque comme une attaque individuelle isolée. Nous sommes préoccupé.es
par les premières déclarations des responsables français qui classent
l’attaque comme un acte individuel de violence à motivation raciale.

Le fait que les 3 cibles étaient kurdes montre qu’il s’agit d’une attaque
organisée et politiquement motivée.

Comment l’assaillant a t-il su qu’il y avait une réunion à l’heure exacte
de l’attaque ? Qui l’a transporté sur le lieu de l’attaque ?
Le suspect aurait-il pu être recruté et radicalisé par des agents turcs
en prison ? Tous ces éléments nous amènent à penser que cet attentat a
été organisé par l’état turc.

Nous exhortons le gouvernement français à assurer une enquête approfondie
sur les circonstances de cet incident car il ne s’agit pas seulement
d’une agression contre le peuple kurde mais contre tous les peuples
d’europe et sa démocratie.

Lorsque l’inhumain ISIS (état islamique) a attaqué Paris et d’autres
villes européennes, les hommes et les femmes kurdes du YPG et du YPJ
ont mené le combat et ont sacrifié plus de 12 000 vies dans la défaite
finale du califat.
Nous appelons les peuples d’europe et du monde à défendre le droit à la
vie et à la liberté du peuple kurde. Où qu’ils soient, le peuple kurde
et ses amis doivent rester unis face à ces attaques et doivent appeler
le gouvernement français à assumer ses responsabilités.
L’état turc doit être tenu pour responsable et doit être jugé pour ses crimes.

Nous demandons instamment à la france et à l’union européenne de se réunir
immédiatement sous le titre d’opération terroriste de l’état turc sur son
sol, et de prendre position contre ; et de discuter des opération turques
sur son sol, et de prendre position contre ces actes de terrorisme."


.. note:: Dans la nuit du 9 au 10 janvier 2013, Fidan Dogan, Sakine Cansiz et
    Leyla Saylemez toutes engagées au sein du Parti des travailleurs du
    Kurdistan (PKK), avaient été abattues rue Lafayette, dans le 10e arrondissement.

    Le suspect présumé, Omer Güney, avait été interpellé dans la foulée et
    écroué pour « assassinats en relation avec une entreprise terroriste ».
    Atteint d’une tumeur célébrale, il est mort à Paris en décembre 2016,
    quelques mois avant son procès devant la Cour d’assises spéciale.


