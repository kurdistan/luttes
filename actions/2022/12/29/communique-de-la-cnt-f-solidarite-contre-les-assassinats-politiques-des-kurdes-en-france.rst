
.. _cnt_si_2022_12_29:

======================================================================================================================================================================
2022-12-29 Communiqué de la CNT-F : Solidarité contre les assassinats politiques des Kurdes en France ! Stop à la complicité du pouvoir français avec l’État turc !
======================================================================================================================================================================

- https://international.cnt-f.org/Communique-de-la-CNT-F-Solidarite-contre-les-assassinats-politiques-des-Kurdes-en-France.html
- https://international.cnt-f.org/IMG/pdf/kurd_comm_si_dec2022_vfinal.pdf
- :ref:`retranscription_cnt_2022_12_23`

.. figure:: images/les_6_tuees.png
   :align: center

Introduction
==============

C’est avec un terrible effroi, une profonde tristesse et une colère noire
que nous avons appris vendredi 23 décembre 2022 la mort de nos trois
camarades kurdes tués par balles, ainsi que l’état de santé grave de
trois autres camarades.

**La CNT-F présente ses plus sincères condoléances et son entière solidarité
aux familles des victimes, aux proches et amis, à la communauté kurde
en France, aux camarades du Conseil Démocratique Kurde en France (CDK-F),
et à tous les militant·es kurdes en France et celles et ceux qui combattent
au Kurdistan.**

L’homme qui a ouvert le feu au sein du Centre culturel Kurde, siège du
CDK-F, au cœur du Xème arrondissement de Paris, était connu pour des
faits de violences ciblant des étrangers.
Il venait de sortir de prison et était en attente de jugement pour
l’attaque au sabre commise en décembre dernier à Paris.
L’extrême droite assassine ici comme ailleurs, et partout le gouvernement
et certains médias leur offrent des espaces de paroles et de pouvoir.

L’État français ne considère pas les attaques d’extrême droite au même
titre que les autres attentats et il ne reconnaîtra jamais qu’il laisserait
des services secrets étrangers agir en toute impunité sur le sol français.

**Nous rejoignons les positions des camarades kurdes : l’État turc de Recep
Tayyip Erdoğan a certainement orchestré cet attentat, ce sont des assassinats
politiques !**

Pour l’heure, la justice française ne retient que la qualification de
« racisme » pour instruire le procès de l’assassin.
Cela montre le déni et le mépris vis-à-vis des proches des victimes, et
des représentants du CDK-F dont le siège a été attaqué.

Iels qualifient de terroriste l’attentat qui vient d’avoir lieu, en lien
avec des éléments précis et circonstanciés qu’ils mettent en avant, et
souhaitent que cela soit pris en compte dans l’enquête.

**En effet, ces meurtres sont commis à quelques jours de la commémoration
des 10 ans du triple assassinat de trois militantes kurdes:**

- :ref:`Sakine Cansız <sakine_cansiz>` |SakineCansiz|, 
- :ref:`Fidan Doğan <fidan_dogan>` |FidanDogan|
- et :ref:`Leyla Şaylemez <leyla_soylemez>` |LeylaSoylemez|

perpétré dans le 10ème  arrondissement de Paris dans la nuit du 9 au 10
janvier 2013, assassinat  commandité par l’État turc avec la complicité
du pouvoir français.

Assassinat pour lequel il n’y a toujours ni vérité ni justice !

Point essentiel, les tirs ont débuté exactement au moment où devait
normalement débuter une réunion nationale du mouvement des femmes kurdes
en France, rassemblant pas moins de quatre-vingt personnes.

Réunion qui devait préparer la grande manifestation du 7 janvier prochain
pour la commémoration des 10 ans du triple assassinat des trois femmes
kurdes en janvier 2013.

Cette réunion a été décalée au dernier moment d’une heure en raison des
problèmes de transport ce jour-là.
Cela aurait pu se traduire en véritable massacre de plusieurs dizaines
de militantes kurdes. Un féminicide de plusieurs femmes aurait aussi
été l’objectif du tueur.

Objectif qu’il a atteint quand même : :ref:`Evîn Goyî (Emine Kara) <emine_kara>` |EmineKara|.
Assassinée au Centre kurde, elle était une responsable du mouvement des femmes kurdes
en France au niveau international. Elle était depuis longtemps une
combattante au Rojava. Au front, les armes à la main, contre l’Etat islamique.
Elle avait été blessée à Raqqa. Elle était venue en France se faire soigner.
Sa demande d’asile avait été rejetée par les autorités françaises.
**Elle était très impliquée dans la solidarité au soulèvement en Iran.**

**Blessée par balle sur le palier du Centre kurde, le tueur l’a poursuivie
à l’intérieur pour l’exécuter**.

|AbdurrahmanKizil| :ref:`Abdurrahman Kizil <abdurrahman_kizil>`, assassiné au Centre kurde, était un militant âgé et
infatigable. Présent à toutes les occasions militantes organisées, et
quasi quotidiennement au Centre kurde. Il a voué toute sa vie à la cause
de son peuple.

|MirPerwer|  :ref:`Mîr Perwer <mir_perwer>`, assassiné au restaurant en face du Centre Kurde, était un
jeune artiste, chanteur et musicien, engagé pour la cause de son peuple,
et connu par la communauté kurde. Il avait été emprisonné en Turquie
car il chantait dans sa langue, le kurde, et ses paroles dérangeaient
le pouvoir turc. Il résidait en France depuis plusieurs années, il avait
le statut de réfugié politique. Il fréquentait très souvent le Centre Kurde.

Comme :ref:`Sakine <sakine_cansiz>` |SakineCansiz|, :ref:`Fidan <fidan_dogan>` |FidanDogan| et :ref:`Leyla <leyla_soylemez>` |LeylaSoylemez|,
**nous ne les oublierons jamais**.

La communauté kurde est encore une fois meurtrie, nous partageons votre
peine et votre douleur camarades kurdes.

Nous tenons à exprimer notre profonde colère face au mépris et à la
répression du pouvoir français vis à vis de la communauté kurde endeuillée
aujourd’hui :

- En effet ce vendredi 23 décembre, un rassemblement se tenait à proximité
  du lieu dans l’après-midi, dans le calme et surtout dans une sidération
  glaciale, et dans l’attente.
  Arrivé sur place, le ministre de l’Intérieur Darmanin, a non seulement
  refusé d’échanger avec les représentants du CDK-F dont le siège a été
  la cible, mais il a en plus déclaré aux médias à quelques mètres de la
  foule que la personne qui sortait de prison n’était pas connu des
  services de renseignements !
  Très vite, la police présente a chargé violemment les personnes rassemblées
  qui ont hué et exprimé leur colère à ce moment précis.
  Le rassemblement a été dispersé par les lacrymogènes et les matraques !
  Le choc, la sidération et en plus la matraque... Nous dénonçons fermement
  la répression qui s’est abattue pendant plusieurs heures dans le quartier
  sur une communauté endeuillée !

- Le lendemain, samedi 24 décembre, un rassemblement suivi d’une manifestation
  avait été appelé par le CDK-F, à Place de la République.
  Manifestation déposée en préfecture et autorisée. Un petit groupe d’extrême
  droite turque, les loups gris, sont venus provoquer le rassemblement de
  plusieurs milliers de Kurdes et leurs soutiens.
  La jeunesse kurde en colère a voulu les dégager. La police qui avait
  bouclé la place, a rapidement et abondamment gazé les manifestants et
  mis en sécurité derrière eux le groupe des loups gris.
  Par ailleurs, à la fin du rassemblement, la police a totalement empêché
  que la manifestation puisse se mettre en marche. Les Kurdes n’ont pas
  pu faire leur manifestation et ont dû écourter les prises de paroles
  prévues en raison de l’importance des gaz lacrymogènes sur la place.

Nous exprimons toute notre solidarité avec la jeunesse kurde qui a témoigné
sa colère de ne pouvoir marcher comme prévu ce jour-là.
Nous dénonçons encore une fois cette répression, et les nombreuses
interpellations qui ont eu lieu. Comme nous dénonçons la répression qui
a pu avoir lieu dans d’autres villes comme à Marseille.

**Nous dénonçons fermement la complicité du pouvoir français avec l’État turc :**

- Il n’y a toujours pas vérité et justice pour les assassinats de Sakine,
  Fidan, et Leyla. Depuis 10 ans, les autorités françaises refusent de
  lever le secret défense sur ces dossiers, empêchant l’avocat des
  victimes à y accéder.
  Les familles des victimes n’ont jamais été reçues par les autorités
  françaises. **Nous exigeons la levée de ce secret défense !**

- Nous dénonçons le harcèlement et la répression que vivent régulièrement
  les militants kurdes en France par les autorités françaises, via perquisitions
  répétées, arrestations, poursuites judiciaires.
  Nous nous rappelons qu’en 2019, année de la visite du ministre de la
  Défense Le Drian en Turquie, l’Etat français avait organisé enquêtes,
  fouilles et interpellations sur les organisations kurdes françaises.
  Pourquoi alors une telle agitation ? En échange de quoi ?

- Nous dénonçons le refus des autorités françaises de reconnaître le statut
  de réfugiés aux Kurdes, ainsi que leur complicité de fait avec les
  politiques discriminatoires et criminelles de l’État turc et ses
  exactions contre le peuple kurde !

- Les camarades kurdes reçoivent régulièrement des menaces ici en France,
  et ont alerté maintes fois de leurs inquiétudes.
  Nous nous rappelons en 2019 lorsque deux camarades, Agit Polat (porte-parole du CDK-F)
  et Vedat Bingol (ancien co-président du CDK-F) avaient reçu explicitement
  des menaces de mort, et avaient alerté les autorités.
  A cette même période, leurs comptes avaient été gelés par le ministère
  de l’Economie et de l’Intérieur français. Ces menaces ne sont
  malheureusement pas isolées, les camarades kurdes y sont confrontés
  trop souvent. Agit Polat a alerté 20 jours avant cet attentat l’inquiétude
  et la volonté de sécurité aux abords de leur centre. La non réaction
  aux demandes et alertes des militants kurdes, fait partie de la complicité
  des autorités françaises avec l’État turc.

Aujourd’hui de nouveaux morts, et des blessés ! C’est horrible, et inacceptable !
Il est urgent de construire une réaction à la hauteur de la gravité de ces actes !

**La CNT-F via son secrétariat international appelle à se mobiliser et
rejoindre massivement la manifestation du 7 janvier 2023 à 10h à Gare
du Nord à Paris pour la vérité et justice pour Sakine, Fidan et Leyla,
10 ans après leur assassinat, mais aussi pour Evin, Abdurrahman et Mir…**

Exprimons ensemble notre colère et notre solidarité !

**Camarades Kurdes nous serons toujours à vos côtés !**

Votre combat pour un Kurdistan libre est à jamais le nôtre.
Nous continuerons de lutter ensemble pour la construction d’un autre
futur débarrassé du capitalisme, du colonialisme, de l’impérialisme,
du fascisme, du racisme et du patriarcat, et pour la liberté.

Nous voulons saluer avec humilité et avec force les prisonniers et
prisonnières politiques Kurdes qui payent de leur liberté leurs convictions
anticapitalistes.

Nous n’oublions pas celles et ceux tombés depuis 40 ans dans votre combat
pour la liberté de votre peuple, opprimé par quatre États impérialistes,
et avec la complicité des grandes puissances de ce monde, dont la
république française qui cautionne et entretient des relations étroites
avec le pouvoir assassin du régime d’Erdoğan.

Nous exprimons toute notre solidarité aux militantes et militants du
mouvement de Libération du Kurdistan actuellement en lutte contre le
fascisme, quelque soit son drapeau !

**La solidarité internationale sera toujours notre arme !
Vive la lutte du peuple Kurde
Biji Kurdistan**

Le Secrétariat International de la CNT-F
Paris, le 29 décembre 2022.


.. note:: Le Conseil démocratique kurde en France (CDK-F) est une
   structure qui regroupe et fédère 24 associations de la diaspora kurde
   à travers toute la France.
   Le CDK-F a pour mission d’aider au rapprochement des membres de la
   communauté kurde, de sauvegarder et d’améliorer la langue de celle-ci,
   son identité et ses valeurs, de défendre ses libertés, ses droits sociaux,
   culturels, économiques et politiques, tout en respectant ceux des
   autres communautés.
   Depuis sa création en 1994, le CDK-F œuvre pour faire connaître les
   Kurdes, leur culture, leur langue, leur situation politique dans
   chacun des pays qui se divisent le Kurdistan.
   Principale organisation représentative des Kurdes en France, il mène
   des actions au niveau national pour la reconnaissance des droits
   légitimes du peuple kurde et pour dénoncer toutes les violations
   dont celui-ci est victime.

:ref:`Voir CDKF A propos <cdk_fr_a_propos>`

