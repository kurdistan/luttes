
.. _je_suis_evin:

==============================
2022-12-30 **Je suis Evin**
==============================

- :ref:`emine_kara`
- http://twitter.076.ne.jp/reseauserhildan/status/1608774462169841667#m


.. figure:: images/je_suis_evin.jpg
   :align: center

Le mouvement des femmes kurdes appelle à des rassemblements aujourd’hui
avec pour slogan « Je suis Evîn » afin de demander justice pour toutes
les militantes kurdes assassinées et dont les meurtriers et commanditaires
n’ont jamais été jugés.

Nous appelons toutes les femmes à participer au rassemblement que nous
organiserons : ➡️Vendredi 30 décembre 2022 🔴Métro Invalides à 14h

#JinJiyanAzadi #Paris #VéritéEtJustice #9janvier #23décembre
