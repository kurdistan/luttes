
.. _kendal_2018_02_10:

==============================================================================================
**Kendal Breizh, internationaliste libertaire tombé au Rojava** par le Monde Libertraire
==============================================================================================

- https://monde-libertaire.fr/?articlen=7655


Introduction
================================================================

**Dans une volonté à la fois d'hommage et de réflexion**, cet article retranscrit
le cheminement politique et pratique de Kendal Breizh.

Cet anarchiste bretonnant a marqué son milieu par son combat: libérer les
populations et leurs cultures opprimées par l'Etat colonial, que ce soit
en France, au Moyen-Orient ou partout sur le globe.

Kendal Breizh: un anarcho-indépendantiste breton
========================================================

Le 10 février 2018 mourrait Olier dit Kendal Breizh, militant anarcho-indépendantiste
breton. Ce qualificatif peut laisser croire à un détournement nationaliste et
identitaire de la pensée anarchiste mais la réalité est toute autre. S'il
se considère ainsi, c'est pour deux raisons: L'existence du fait colonial et
l'existence d'éléments centralisateurs et jacobins dans le mouvement anar
(qu'il lui arrive d'appeler anarcho-jacobins ou anarcho-nationalistes). Le
courant qu'il défend prône le droit à l'indépendance de tous les peuples
opprimés et dominés du monde et non seulement le peuple breton, il ne peut
être qualifié de 'nationaliste': "en plus de vouloir l'indépendance du
peuple breton vis-à-vis de l'État français, je veux la libération de
tous les peuples et dans tous les domaines (social, colonial, individuel,
culturel); c'est là le fondement même de l'internationalisme." écrit-il
dans un article du N°12 de la revueHuchoer. Pour lui, il faut concilier
la libération des peuples (anticolonialisme et anti-impérialisme) avec la
libération de tous les individus (antisexisme, antiétatisme, autogestion,
antirépression, antiautoritarisme) sans se perdre dans l'interclassisme ou
dans l'indépendance comme fin en soi. Kendal, au-delà de la cause bretonne
s'intéressait, entre autres, aux luttes basques, irlandaises, palestiniennes,
zapatistes et kurdes. C'est ainsi qu'il s'engage au Rojava en 2017 et meurt
sous les bombardements de l'armée turque à Afrine en février 2018.

Le Syndicalisme, un allié ?
=================================

Olier s'intéressa au syndicalisme alliant à la fois revendications sociales
et lutte pour l'autonomie des peuples. Il se pencha particulièrement
sur l'Espagne de 1936 où la révolution se joua sur fond d'identités
régionales (Pays basque, Catalogne) luttant contre le joug de l'Etat
central castillan. D'ailleurs, plusieurs combattants ayant participé à la
révolution espagnole étaient membres de l'Irish Citizen Army. Cette milice
ouvrière de l'ITGWU, syndicat irlandais luttant contre la double oppression
du prolétariat en Irlande: l'exploitation capitaliste de la bourgeoisie
irlandaise et l'exploitation coloniale de l'Empire britannique.

Ce syndicat s'illustra notamment dans la grande grève de 1913, mouvement massif
autant détesté par la Couronne britannique que par les nationalistes du Sin
Fein et dont le seul soutien a été les organisations ouvrières anglaises!

De nouveau, durant la première guerre mondiale, l'ITGWU réussit à stopper
la conscription britannique en Irlande grâce à une autre grève générale
en 1918. Ainsi, ce syndicat révolutionnaire s'inscrivant dans la lutte des
classes contribua à l'indépendance de l'Irlande qui fut malheureusement
récupérée par la bourgeoisie et les nationalistes irlandais.

Si Kendal perçoit le syndicalisme comme une stratégie utile pour la révolution
et pour l'indépendance, cela ne l'empêche pas d'avoir une analyse critique. En
effet, il n'hésite pas à dénoncer la bureaucratie et l'hypocrise de la CGT
ou de la CFDT.

Antimilitariste oui, pacifiste non!
==========================================

Dans de nombreux articles, Olier s'attache à dénoncer le militarisme
étatique. Ainsi, la fin du service militaire ne signe pas la fin de l'emprise
martiale sur la société vu la police hautement armée et le discours
nationaliste dominant valorisant l'armée, telle la Journée d'Appel et
de Préparation à la Défense: autant d'incarnations de l'Armée dans nos
quotidiens. Ces analyses datant des années 2000 ne peuvent que nous faire
écho à l'heure du SNU...

Son antimilitarisme n'est pas dissociable de ses idées anticarcérales et
antirépressives qui lui tenaient très à coeur.

Néanmoins, si Olier est antimilitariste, il n'est pas pacifiste. Il considère
la lutte armée décoloniale comme un moyen de libération de l'individu. Il
crache sur les libertaires dogmatiques qui seraient prêt-es à rejoindre la
bourgeoisie contre un mouvement d'émancipation usant de la violence, alors
qu'il considère celle-ci comme pouvant être bénéfique pour les peuples
colonisés ou dans les manifs. Cependant, il est conscient du risque des
dérives contrerévolutionnaires et militaristes. Pour Olier, la lutte armée
est un moyen qui, comme les autres, ne doit pas être utilisé dogmatiquement
mais pragmatiquement loin des carcans hiérarchiques de l'Armée. C'est dans
cet optique qu'il partit en 2017 combattre au côté des Forces Démocratiques
de Syrie pour défendre l'arme au poing la révolution au Rojava, que ce soit
contre Daesh ou la Turquie.

Colonialisme sur le bout de la langue
===========================================

Dans son anarcho-indépendantisme, la protection des langues et des cultures
est un pan important.

Il ne s'agit pas pour une population de se couper de celles qui ne partagent
pas sa langue ou sa culture mais de rompre avec les Etats dont la pratique
colonialiste impose langue, culture et lois considérées comme supérieures.

La valeur d'une langue n'existe que par celleux qui l'utilisent et qui défendent
leurs valeurs. Il faut donc refuser tous les impérialismes linguistiques
même l'esperanto: l'uniformisation qu'elle soit capitaliste, colonialiste ou
"anarchiste" est néfaste pour les individu-es.

Une langue peut tout à fait être victime et oppresseure tel le français qui
se défend face à l'anglais en minorant d'autres langues d'où la nécessité
d'éviter l'écueil de la promotion d'une langue contre les autres mais de
favoriser la liberté d'exister pour toutes.

"Toute langue étant le reflet d'une culture et d'une expression populaire
passée ou présente, ancienne ou récente - l'espéranto par exemple -,
doit pouvoir perdurer, être transmise et sauvegarder tant qu'il existe un ou
plusieurs individus qui le désirent. Il n'existe pas de langue civilisatrice
ou sauvage, de langue démocratique ou fasciste, de langue libératrice ou
aliénante, bref de langues supérieures ou inférieures: dire le contraire,
c'est se lancer dans le chemin fascisant du centralisme ethnique." écrit
Kendal dans le N°11 d'Huchoer

En France, l'équation de l'Etat-Nation: Etat=Nation=Culture=Langue se sent
menacée par les différentes cultures et langues sur son territoire, qu'elles
soient autochtones ou issues des migrations, ce qui n'empêche pas la France
de prôner le français en dehors de ses frontières.

Cet universalisme rejette les différences pour les supprimer, Olier lui
oppose l'internationalisme et la tolérance qui acceptent les différences
dans la solidarité.

Défendre la culture et les langues n'est pas défendre corps et âmes des
traditions dont les aspects oppressifs notamment patriarcaux doivent être
remis en cause. Selon lui cette défense ne peut qu'être populaire: se
placer contre ces oppressions culturelles est indissociable de l'opposition
à l'oppression des peuples, les oppressions des populations étant bien plus
inacceptables. "Le centralisme colonial est l'ennemi majeur du fédéralisme
libertaire" dit-il dans le N°12 d'Huchoer.

Pourquoi cet article?
==============================

Ces lignes s'appuient majoritairement sur ses articles dans l'Huchoer.

Elles rendent hommage à un militant internationaliste parmi tant d'autres
dont l'idéologie personnelle se rapproche de la nôtre malgré certaines
inimitiés entre notre groupe et certains groupes dont il a fait partie,
inimitiés qu'il conviendrait de dépasser aujourd'hui pour un internationalisme
libertaire d'autant qu'à l'occasion des six ans de sa mort, se tient le 10
février 2024 à Rennes une journée autour de l'internationalisme: Bevañ
an Etrebroadelouriezh (Vivre l'Internationalisme en breton) organisée par
les ami-es, camarades et la famille d'Olier, Serhildan-Bretagne et le CDK-R
(Centre Démocratique Kurde-Rennes).
