
.. _justice_2023_01_07:

=================================================================
2023-01-07 **Justice pour les militants kurdes tués à Paris**
=================================================================

- https://serhildan.org/portfolio-marche-pour-la-verite-et-la-justice-a-paris-07-janvier-2023/


2022-12-23 |AbdurrahmanKizil| #AbdurrahmanKızıl |EmineKara| #EmineKara  |MirPerwer| #MirPerwer
================================================================================================================

- :ref:`assassinats_2022_12_23`


2013-01-09 **Triple assassinat de militantes kurdes #FidanDogan #SakineCansiz #LeylaSoylemez**
==================================================================================================

- :ref:`assassinats_2013_01_09`


.. figure:: images/notre_vengeance.png
   :align: center


.. figure:: images/berivan.png
   :align: center

   :ref:`Berivan Firat <berivan_firat>` prend la parole à l’occasion du
   10e anniversaire du meurtre de 3 femmes kurdes à Paris le 9 janvier 2013 par 1 espion turc


.. figure:: images/verite_et_justice.png
   :align: center

   Vérité et Justice


.. figure:: images/habit_femme_kurde.png
   :align: center


.. figure:: images/hommage_aux_tuees.png
   :align: center


- http://bird.trom.tf/Berivanfirat75/status/1611305191487176706#m

UN APPEL À MES COMPATRIOTES #FRANÇAIS : Lorsque trois #Kurdes ont été de
nouveau tués à #Paris, les Kurdes ont été meurtries et certains étés
très en colère.
Demain,accompagnons les kurdes. Il est temps de dire stop l'impunité des
crimes des services secrets turcs, sur notre sol.


- http://bird.trom.tf/Berivanfirat75/status/1611745968654802952#m

Merci Monsieur le Vice-bâtonnier de Paris @vincent_niore de votre présence.
Votre message de solidarité a été entendu par des dizaines de milliers
de personnes,rassemblées pour exiger la justice.
#SakineRojbinLeyla.


- http://bird.trom.tf/Berivanfirat75/status/1612024053614710784#m

M. l'ambassadeur,en 2013,votre président et les responsables français,
c'étaient empressés de dire :"règlement interne, l'histoire d'un fou"
Vous aviez monté un roman pour dissimuler la responsabilité turc.
Jusqu'à ce qu'il s'avère qu'il s'agit de votre agent,missionné par le MIT.



- https://qoto.org/@kurdistanluttes/109653364874888349

#Turquie #Russie #menace

Romain Mielcarek, spécialiste reconnu des questions internationales et
de sécurité, classe la #Turquie dans les principales menaces d'espionnage
sur le sol 🇫🇷. L'expert relève l'agressivité des services 🇹🇷.

Source: blast, Romain Mielcarek

- https://video.blast-info.fr/w/v8Ur6cqJfbhQF1gvyzg8Wk

Romain Mielcarek est journaliste indépendant spécialiste des questions
internationales et de sécurité. Il y a dix ans, les services secrets
russes ont essayé de le recruter à des fins de renseignement.
Profitant de l'opportunité, il a , plusieurs années durant, analysé et
documenté la manière dont l'espionnage russe tente de tirer son épingle
du jeu dans l'hexagone.
Aujourd'hui, il sort "Les Moujiks - La France dans les griffes des espions
russes" aux éditions Denoël, fruit de dix ans d'enquête dans les milieux
de l'influence russe et du contre-espionnage français.

Reportage humanité
====================

- https://ytb.trom.tf/watch?v=xJcg37E4Eqs
- http://bird.trom.tf/Berivanfirat75/status/1611464097547292672#m

Ce reportage de l'humanité vous explique le triple assassinat de #2013
et pourquoi les #Kurdes maintiennent que l'attaque du   23/12, du rue
d'Enghien est une attaque terroriste "commandé" par la 🇹🇷
