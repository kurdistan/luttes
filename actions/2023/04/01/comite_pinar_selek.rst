

=====================
pinar selek
=====================

::

    Ligue des droits de l'Homme
    Section de Nice
    contact-mails : liguenice+contact@gmail.com
    Internet : http://site.ldh-france.org/nice/
    Facebook : www.facebook.com/ldhnice
    Instagram : liguedesdroitsdelhommenice
    Tweeter : @liguenice
    Diaspora : ldh_nice@diaspora-fr.org
    Mastodon : @ldh_nice@piaille.fr
    courrier : Maison des associations 12, ter Pl Garibaldi 06300 Nice
    --
    Ce service de liste de discussion vous est offert gracieusement par l'association Framasoft.
    N'hesitez pas a lui faire un don pour qu'elle puisse poursuivre ses actions.
    https://soutenir.framasoft.org

    Vous recevez cet email car vous etes abonne.e a la liste "comitepinar".
    Pour vous desabonner, merci de cliquer sur ce lien : https://framalistes.org/sympa/sigrequest/comitepinar
    ou d'envoyer un mail a sympa@framalistes.org avec comme sujet "unsubscribe comitepinar"

    You receive this email since you subscribed to the list "comitepinar".
    To unsubscribe, click on following link: https://framalistes.org/sympa/sigrequest/comitepinar
    or send an email to sympa@framalistes.org with the subject: unsubscribe comitepinar
