
.. _jjr_2023_12_01:

=====================================
Message de soutien des JJR au KCK
=====================================

- :ref:`jjr:jjr_kck_2023_12_24`

**Nous remercions le KCK (Union des communautés du Kurdistan) pour avoir adressé
ses condoléances au peuple Juif d’Israël et au peuple Arabe de Palestine
au lendemain de l'attaque du 7 octobre 2023**.
